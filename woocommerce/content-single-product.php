<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div class="card product-side product-side__order product-side__order--mobile is-hidden-tablet">
	<div class="level is-mobile">
		<div class="level-left">
			<div class="level-item product-side__order__price"><?php get_product_price($post->ID); ?></div>
		</div>
		<div class="level-right">
			<div class="level-item product-side__order__btn">
				<?php ets_order_btn(); ?>
			</div>
		</div>
	</div>
</div>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
<div class="section">
		<div class="container">
			<div class="columns is-centered">
				<div class="column is-11-widescreen is-full-tablet">
					<div class="columns">
						<div class="column is-8-widescreen is-7-tablet">
	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action( 'woocommerce_single_product_summary' );
		?>
	</div>
	</div> <!-- .column.is-8 -->

<div class="column is-4-widescreen is-5-tablet">
	<div class="card product-side product-side__order is-hidden-mobile">
		<div class="level is-mobile">
			<div class="level-left">
				<div class="level-item product-side__order__price"><?php get_product_price($post->ID); ?></div>
			</div>
			<div class="level-right">
				<div class="level-item product-side__order__btn">
					<?php ets_order_btn(); ?>
				</div>
			</div>
		</div>
	</div> <!-- .product-side__order -->

	<?php
	
	if (get_field('related_product')) : ?>
	
		<div class="card product-side product-side__related">
			<h4 class="product-side__title">Related Product</h4>

			<div class="product-side__related-product">
				<?php get_template_part('template-parts/section', 'related-course'); ?>
			</div>

		</div> <!-- .product-side__related -->
	<?php endif; ?>


	<?php get_template_part('template-parts/section', 'product-testimonials'); ?>

	<div class="card product-side product-side__help">
		<h4 class="product-side__title">Need help?</h4>

		<div class="product-side__help__info">
			<div class="columns is-mobile">
				<div class="column is-1"><i class="fas fa-question-circle icon--blue"></i></div>
				<div class="column"><a href="/faqs">View our FAQs</a></div>
			</div>
			<div class="columns is-mobile">
				<div class="column is-1"><i class="fas fa-envelope icon--blue"></i></div>
				<div class="column"><a href="mailto:info@essential-training.co.uk">info@essential-training.co.uk</a></div>
			</div>
			<div class="columns is-mobile">
				<div class="column is-1"><i class="fas fa-phone icon--blue"></i></div>
				<div class="column">+44 (0)1279 726800</div>
			</div>
		</div>
	</div> <!-- .product-side__help -->

</div> <!-- .is-4 -->
</div> <!-- .columns -->

<div class="columns">
<div class="column">
	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>

</div>
					</div>

				</div> <!-- .columns.is-9 -->

			</div> <!-- .columns -->
		
		</div> <!-- .container -->
	</div>
</div>


<?php do_action( 'woocommerce_after_single_product' ); ?>

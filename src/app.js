//Elements
const elements = {
	btnStudent: document.querySelector('.btn-student'),
	hiwPoint: document.querySelectorAll('.how-group__points'),
	hiwCardActive: document.querySelectorAll('.how-group__info'),
	openDemoLink: document.querySelector('.open-demo'),
	affOpt: document.querySelectorAll('.aff-opt'),
	body: document.querySelector('body'),
};

//Event Handlers
elements.btnStudent.addEventListener('click', e => {
	window.open(e.target.dataset.href);
});

elements.hiwPoint.forEach(el => {
	el.addEventListener('click', e => {

		const point = e.target.closest('.how-group__points');
		hiw(point);
	});
})

elements.affOpt.forEach(el => {
	el.addEventListener('click', e => {

		if (e.target.matches('.aff-opt--dropdown.active, .aff-opt--dropdown.active *')) {
			document.querySelector('.aff-opt--dropdown.active').classList.remove('active');
			document.querySelector('.aff-opt__content.active').classList.remove('active');
		} else if (e.target.matches('.aff-opt--dropdown, .aff-opt--dropdown *')) { 
	
			if (document.querySelector('.aff-opt--dropdown.active')) {
				document.querySelector('.aff-opt--dropdown.active').classList.remove('active');
				document.querySelector('.aff-opt__content.active').classList.remove('active');
			}
			const dropdown = e.target.closest('.aff-opt--dropdown');
			
			dropdown.classList.add('active');

			const title = e.target.closest('.aff-opt--dropdown').dataset.title;
	
			document.querySelector(`[data-content=${title}]`).classList.add('active');
		}
	})
})


//How it Works func
const hiw = (point) => {
	//Remove current active point
	elements.hiwPoint.forEach(e => {
		if (e.classList.contains('is-active')) {
			e.classList.remove('is-active');
		}
	})

	//Add new active point
	point.classList.add('is-active');


	//Hide current active card
	elements.hiwCardActive.forEach(e => {
		if (e.classList.contains('is-active')) {
			e.classList.remove('is-active');
		}
	})

	//Show new active card
	const activePoint = point.dataset.point;
	document.querySelector(`[data-info="${activePoint}"]`).classList.add('is-active');

}

//General Event Listeners

document.addEventListener('click', e => {
	const elClassList = [...e.target.parentElement.classList];
	
	if (elClassList.includes('prevent-default')) {
		e.preventDefault()
	}

	if ([...e.target.classList].includes('open-demo')) {
		e.preventDefault();
		openDemo(e.target.href,'', 'width=800,height=600')
	}
	
	if ([...e.target.classList].includes('faq-link')) {
		e.preventDefault();
		window.open(e.target.href);
	}
	
	if ([...e.target.classList].includes('menu-item-has-children') || elClassList.includes('menu-item-has-children')) {
		e.preventDefault();
		
		if (document.querySelector('.submenu-open')) {
			document.querySelector('.submenu-open').classList.remove('submenu-open');
		} else {
			if ( e.target.tagName == 'A') {
				e.target.parentElement.classList.add('submenu-open');
			} else if (e.target.tagname == 'LI') {
				e.target.classList.add('submenu-open');
			}
		}
	}
	


})


//Demo page

if (window.location.href.indexOf('?course-demo=') > 0) {
	const courseID = getUrlParameter('course-demo');
	const courseTitle = courseID.replace(/-/g, ' ');
	const hiddenField = document.querySelector('#course-field--hidden');
	const demoText = document.querySelector('.free-download-text');
	const demoTitle = document.querySelector('.hero-body__heading');
	var html, demoURL;

	hiddenField.value = courseTitle;

	switch(courseID) {
		case 'Protection-From-Abuse':
			html = `<p>Please enjoy our short Protection From Abuse online tutorial. This is an excellent opportunity to see if you would enjoy e-learning at no cost, whilst studying a subject that is relevant to all therapists. Both the FHT and AoR award CPD points upon successful completion.</p>`;
			demoURL = 'https://essential-training.co.uk/training/mbp_abuse/index.html';
			break;

		case 'Anatomy-Physiology':
			html = `<p>This free course demonstration provides you with a few screens from every section of the Essential Anatomy and Physiology tutorial.</p>`;
			demoURL = 'https://essential-training.co.uk/training/apdemo06/apdemosept06.html';
			break;

		case 'Fire-Evacuation':
			html = `<p>This free course demonstration provides you with a few screens from the Fire and Evacuation tutorial.</p>`;
			demoURL = 'https://essential-training.co.uk/training/hsinduction/index.html';
			break;

		case 'Health-Safety':
			html = `<p>This free course demonstration shows a few screens from the Health & Safety tutorial.</p>`;
			demoURL = 'https://essential-training.co.uk/training/hslevel3/index.html';
			break;
		default: 
			html = `<p>That free tutorial download doesn't exist. Please try again or <a href="/contact-us/">contact us for help</a>`
			document.body.classList.add('no-course-exists');

	}

	demoText.insertAdjacentHTML('afterBegin', html);

	if ([...document.body.classList].includes('no-course-exists')) {
		demoTitle.insertAdjacentHTML('beforeend', ' - Download doesn\'t exist');
	} else {
		demoTitle.insertAdjacentHTML('beforeend', ' - ' + courseTitle);
	}

	document.addEventListener( 'wpcf7mailsent', function( event ) {
		window.open(demoURL, 'PopupPage', 'height=605,width=805,scrollbars=yes,resizable=no');
		document.querySelector('.free-tut__form').style.display ='none';
		}, false );

}


//Mobile Nav

var $burgers = document.querySelectorAll('.burger');

if ($burgers.length > 0) {
	$burgers.forEach(function ($el) {
		$el.addEventListener('click', function () {
			var target = $el.dataset.target;
			var $target = document.getElementById(target);
			$el.classList.toggle('is-active');
			$target.classList.toggle('is-active');
		});
	});
}

var splitStar = document.querySelectorAll('.split-star');

if (splitStar.length > 0) {
	splitStar.forEach(function (el) {
		var starWidth = el.dataset.splitstar;
		var star = el.dataset.star;
		
		var html = `
		<style>.split-star[data-star="${star}"]:before {width: ${starWidth}%;}</style>
		`
		el.insertAdjacentHTML("afterbegin", html)

	});
}

//promo functionality

if (document.querySelector('.promo__popup')) {
	const promoPopUp = document.querySelector('.promo__popup');

	promoPopUp.style.display = 'block';
	promoPopUp.classList.add('promo__popup--peek');

	document.querySelector('.promo__popup, .promo__popup > h3, .promo__toggle').addEventListener('click', function() {
		if ([...promoPopUp.classList].includes('promo__popup--open')) {
			promoPopUp.classList.remove('promo__popup--open');
			document.querySelector('.promo__toggle i').classList.add('fa-chevron-up');
			document.querySelector('.promo__toggle i').classList.remove('fa-chevron-down');
		} else {
			document.querySelector('.promo__toggle i').classList.remove('fa-chevron-up');
			document.querySelector('.promo__toggle i').classList.add('fa-chevron-down');
			promoPopUp.classList.add('promo__popup--open');
		}
		
	});
}

//Helper Functions

function openDemo(theURL,winName,features) {
	window.open(theURL,winName,features);
	
}

function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

//

//jQuery

(function ($) {
	$(function () {
		var carousels = bulmaCarousel.attach(); // carousels now contains an array of all Carousel instances
		if ($('body').hasClass('page-faqs') || $('body').hasClass('page-ets-affiliate-information')) {
			var accordions = bulmaAccordion.attach(); // accordions now contains an array of all Accordion instances
		}
	});

	$('.form-field__input').focus(function () {
		$(this).parents('.form__group').addClass('focused');
	});

	$('.form-field__input').blur(function () {
		var inputValue = $(this).val();
		if (inputValue == "") {
			$(this).removeClass('filled');
			$(this).parents('.form__group').removeClass('focused');
		} else {
			$(this).addClass('filled');
		}
	});

	//init select 2
	$(document).ready(function () {

		if ($('.product-box--course').length > 0 ) {
			if ($('.product-filter__accreditation').length > 0) {
				$('.product-filter__accreditation').select2({
					placeholder: 'Accreditation',
					minimumResultsForSearch: -1
				});
			}
		} else {
			$('#product-filter').hide();
		}
		 


		//Product page filter

		//accredidation
		$(document.body).on('change', '.product-filter__accreditation', function() {

			var selectedAccred = this.value;
			
			if (selectedAccred === 'all') {
				$('.product-box--course').each(function() {
					$(this).show();
				});
				return;
			}

			$('.product-box--course').not(document.querySelector('[data-accreditation="'+selectedAccred+'"]')).hide();

			$('[data-accreditation="'+selectedAccred+'"]').each(function() {
				$(this).show();	
			})

		});

	});
	

	$(".homepage-testimonials").owlCarousel({
		loop: true,
		nav: true,
		dots: false,
		slideBy: 'page',
		responsive: {
			0 : {
				items: 1
			},
			700 : {
				items: 2
			},
			1000 : {
				items: 3
			}
		}
	});

	$(".homepage-logos").owlCarousel({
		loop: true,
		nav: true,
		dots: false,
		slideBy: 'page',
		responsive: {
			0 : {
				items: 1
			},
			700 : {
				items: 3
			},
			1000 : {
				items: 5
			}
		}
	});


})(jQuery);
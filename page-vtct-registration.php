<?php
/**
 * Template name: VTCT Registration Page
 *
*/
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="section">
				<div class="container default-page__container">
					<div class="columns is-centered">
						<div class="column is-11-desktop is-full-tablet is-paddingless">

						<?php echo do_shortcode('[contact-form-7 id="345" title="VTCT Registration"]'); ?>
						
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package Essential_Training_Solutions
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
 *
 * @return void
 */
function essential_training_woocommerce_setup() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'essential_training_woocommerce_setup' );

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function essential_training_woocommerce_scripts() {

	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}';

	wp_add_inline_style( 'essential-training-woocommerce-style', $inline_font );
}
add_action( 'wp_enqueue_scripts', 'essential_training_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function essential_training_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'essential_training_woocommerce_active_body_class' );

/**
 * Products per page.
 *
 * @return integer number of products.
 */
function essential_training_woocommerce_products_per_page() {
	return 12;
}
add_filter( 'loop_shop_per_page', 'essential_training_woocommerce_products_per_page' );

/**
 * Product gallery thumnbail columns.
 *
 * @return integer number of columns.
 */
function essential_training_woocommerce_thumbnail_columns() {
	return 4;
}
add_filter( 'woocommerce_product_thumbnails_columns', 'essential_training_woocommerce_thumbnail_columns' );

/**
 * Default loop columns on product archives.
 *
 * @return integer products per row.
 */
function essential_training_woocommerce_loop_columns() {
	return 3;
}
add_filter( 'loop_shop_columns', 'essential_training_woocommerce_loop_columns' );

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function essential_training_woocommerce_related_products_args( $args ) {
	$defaults = array(
		'posts_per_page' => 3,
		'columns'        => 3,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'essential_training_woocommerce_related_products_args' );

if ( ! function_exists( 'essential_training_woocommerce_product_columns_wrapper' ) ) {
	/**
	 * Product columns wrapper.
	 *
	 * @return  void
	 */
	function essential_training_woocommerce_product_columns_wrapper() {
		$columns = essential_training_woocommerce_loop_columns();
		echo '<div class="columns-' . absint( $columns ) . '">';
	}
}
add_action( 'woocommerce_before_shop_loop', 'essential_training_woocommerce_product_columns_wrapper', 40 );

if ( ! function_exists( 'essential_training_woocommerce_product_columns_wrapper_close' ) ) {
	/**
	 * Product columns wrapper close.
	 *
	 * @return  void
	 */
	function essential_training_woocommerce_product_columns_wrapper_close() {
		echo '</div>';
	}
}
add_action( 'woocommerce_after_shop_loop', 'essential_training_woocommerce_product_columns_wrapper_close', 40 );

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

if ( ! function_exists( 'essential_training_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function essential_training_woocommerce_wrapper_before() {
		?>
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
			<?php
	}
}
add_action( 'woocommerce_before_main_content', 'essential_training_woocommerce_wrapper_before' );

if ( ! function_exists( 'essential_training_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function essential_training_woocommerce_wrapper_after() {
			?>
			</main><!-- #main -->
		</div><!-- #primary -->
		<?php
	}
}
add_action( 'woocommerce_after_main_content', 'essential_training_woocommerce_wrapper_after' );

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
	<?php
		if ( function_exists( 'essential_training_woocommerce_header_cart' ) ) {
			essential_training_woocommerce_header_cart();
		}
	?>
 */

if ( ! function_exists( 'essential_training_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function essential_training_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		essential_training_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'essential_training_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'essential_training_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function essential_training_woocommerce_cart_link() {
		?>
		<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'essential-training' ); ?>">
			<?php
			$item_count_text = sprintf(
				/* translators: number of items in the mini cart. */
				_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'essential-training' ),
				WC()->cart->get_cart_contents_count()
			);
			?>
			<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span> <span class="count"><?php echo esc_html( $item_count_text ); ?></span>
		</a>
		<?php
	}
}

if ( ! function_exists( 'essential_training_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function essential_training_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php essential_training_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}


//Remove woocommerce options
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20, 0);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5, 0);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10, 0);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20, 0);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30, 0);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40, 0);
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10, 0);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );


//As Woo tabs have been removed, need to add description back in
function woocommerce_template_product_description() {
	wc_get_template( 'single-product/tabs/description.php' );
  }
  add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );

// Remove description h2
add_filter('woocommerce_product_description_heading', '__return_null');

// Get product price
function get_product_price($post_id) {

	$currency = get_woocommerce_currency_symbol();
	$price = 0;

	if (get_post_meta($post_id, '_sale_price', true)) {
		$price = get_post_meta($post_id, '_sale_price', true); 
	} else {
		$price = get_post_meta($post_id, '_regular_price', true);
	}

	echo $currency, $price;
}

//Custom order now button

function ets_order_btn() {
	global $product; ?>

	<a href="<?php echo $product->add_to_cart_url(); ?>" class="btn btn--white btn--hover-yellow">Order Now</a>
	
<?php }


//Find out if current product is a course product
function ets_is_course() {
	global $post;
	$terms = get_the_terms( $post->ID, 'product_cat' );
	foreach ($terms as $term) {
		if ($term->slug == 'courses') {
			return true;
		}
	}
}

//Find out if cart is empty
function ets_is_cart_empty() {
	return WC()->cart->get_cart_contents_count() == 0;
}
function ets_cart_count() {
	return WC()->cart->get_cart_contents_count();
}

//Add basket link to menu
function ets_add_cart_to_menu($items) {
	// if (!ets_is_cart_empty()) {

	// 	$items .= '';
	// 	return $items; 
	// }
    $count = WC()->cart->cart_contents_count;
    
    if ( $count > 0 ) { 
       $items .= '<li><a href="/basket" title="View basket contents" class="navbar-item basket-link"><i class="fas fa-shopping-basket"></i> <span class="is-hidden-desktop">Basket</span><span class="basket-count">'. $count . '</span></a></li>';
  
    }
    return $items;
}

add_filter('wp_nav_menu_items', 'ets_add_cart_to_menu', 10, 2);


function ets_get_qualification($arr) {
	if ($arr['cert_dip'] === 'certificate') {
		return $arr['cert_qm']['tqt'] . " <div class=\"tooltip\">TQT<span class=\"tooltiptext\">Total Qualification Time</span></div>" . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $arr['cert_qm']['glh'] ." <div class=\"tooltip\">GLH<span class=\"tooltiptext\">Guided Learning Hours</span></div> ";
	} else if ($arr['cert_dip'] === 'diploma') {
		return $arr['diploma_qm']['cpd'] . " <div class=\"tooltip\">CPD<span class=\"tooltiptext\">Continuing Professional Development</span></div>";
	}
}

function ets_display_accreditor($accreditor) {
	return "/wp-content/uploads/accreditor-" . $accreditor . ".jpg";
}


// Change placeholder and label text
function rename_wc_checkout_fields( $fields ) {
	$fields['billing']['billing_company']['label'] = 'Company name';
	return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'rename_wc_checkout_fields' );

function display_product_type($ids) {
	/********************
	 * Category Type IDs
	 * ******************
	 * Courses = 22
	 * Manuals = 25
	 * Revision Tools = 24
	 */

	foreach ($ids as $id) {
		switch($id) {
			case 22 :
				return "Course";
				break;
			case 25 :
				return "Manual";
				break;
			case 24 :
				return "Revision Tool";
				break;
		}
	}


}

//Amend product columns 

add_filter( 'manage_edit-product_columns', 'amend_product_cols' );

function amend_product_cols($columns){
	$columns['accreditation'] = __('Accreditation');

	return $columns;
}

add_action('manage_product_posts_custom_column', 'add_accred_to_col');

function add_accred_to_col($column) {
	if ('accreditation' === $column) {
	
		global $post;

		if (get_field('course_group', $post->ID)) {
			$product_info = get_field('course_group', $post->ID);
			?>
				<img style="max-width:150px; max-height:75px;" src="<?php echo ets_display_accreditor($product_info['accreditation']); ?>"	alt="<?php echo $product_info['qualification_title']; ?>" />
		
		<?php
		}
	}
}

//Remove Upsell display, Result count, Cross sell, Original Price position
remove_action('woocommerce_after_single_product_summary','woocommerce_upsell_display',  15);
remove_action('woocommerce_cart_collaterals','woocommerce_cross_sell_display');
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


function woocommerce_maybe_add_multiple_products_to_cart( $url = false ) {
	// Make sure WC is installed, and add-to-cart qauery arg exists, and contains at least one comma.
	if ( ! class_exists( 'WC_Form_Handler' ) || empty( $_REQUEST['add-to-cart'] ) || !empty( $_REQUEST['discount'] ) ) {
			return;
	}

	// Remove WooCommerce's hook, as it's useless (doesn't handle multiple products).
	remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'add_to_cart_action' ), 20 );
	$add_to_cart_string = $_REQUEST['add-to-cart'] . ',';
	$product_ids = explode( ',', $add_to_cart_string );
	$count       = count( $product_ids );
	$number      = 0;


	foreach ( $product_ids as $id_and_quantity ) {
			// Check for quantities defined in curie notation (<product_id>:<product_quantity>)
			// https://dsgnwrks.pro/snippets/woocommerce-allow-adding-multiple-products-to-the-cart-via-the-add-to-cart-query-string/#comment-12236
			$id_and_quantity = explode( ':', $id_and_quantity );
			$product_id = $id_and_quantity[0];

			$_REQUEST['quantity'] = ! empty( $id_and_quantity[1] ) ? absint( $id_and_quantity[1] ) : 1;

			if ( ++$number === $count ) {
					// Ok, final item, let's send it back to woocommerce's add_to_cart_action method for handling.
					$_REQUEST['add-to-cart'] = $product_id;

					return WC_Form_Handler::add_to_cart_action( $url );
			}

			$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product_id ) );
			$was_added_to_cart = false;
			$adding_to_cart    = wc_get_product( $product_id );

			if ( ! $adding_to_cart ) {
					continue;
			}

			$add_to_cart_handler = apply_filters( 'woocommerce_add_to_cart_handler', $adding_to_cart->get_type(), $adding_to_cart );

			// Variable product handling
			if ( 'variable' === $add_to_cart_handler ) {
					woo_hack_invoke_private_method( 'WC_Form_Handler', 'add_to_cart_handler_variable', $product_id );

			// Grouped Products
			} elseif ( 'grouped' === $add_to_cart_handler ) {
					woo_hack_invoke_private_method( 'WC_Form_Handler', 'add_to_cart_handler_grouped', $product_id );

			// Custom Handler
			} elseif ( has_action( 'woocommerce_add_to_cart_handler_' . $add_to_cart_handler ) ){
					do_action( 'woocommerce_add_to_cart_handler_' . $add_to_cart_handler, $url );

			// Simple Products
			} else {
					woo_hack_invoke_private_method( 'WC_Form_Handler', 'add_to_cart_handler_simple', $product_id );
			}
	}
}

// Fire before the WC_Form_Handler::add_to_cart_action callback.
add_action( 'wp_loaded', 'woocommerce_maybe_add_multiple_products_to_cart', 15 );


/**
* Invoke class private method
*
* @since 0.1.0
*
* @param string $class_name
* @param string $methodName
*
* @return mixed
*/
function woo_hack_invoke_private_method( $class_name, $methodName, $product_id ) {
	if ( version_compare( phpversion(), '5.3', '<' ) ) {
		throw new Exception( 'PHP version does not support ReflectionClass::setAccessible()' );
	}

	$args = func_get_args();
	unset( $args[0], $args[1] );
	$reflection_method = new ReflectionMethod( $class_name, $methodName );
	$reflection_method->setAccessible( true );
	$args = array_merge( array( $reflection_method ), $args );
	
	return $reflection_method->invoke( null, $product_id );
}


// add_action( 'woocommerce_before_calculate_totals', 'checkout_for_affiliate');

//Add Affiliate as custom order data

// Store custom data in cart item
add_action( 'woocommerce_add_cart_item_data','save_custom_data_in_cart', 20, 2 );

function save_custom_data_in_cart( $cart_item_data, $product_id ) {
	//take 'add-to-cart' query and split at comma
	$add_to_cart_string = $_REQUEST['add-to-cart'];
	
	$product_ids = explode( ',', $add_to_cart_string );
	
	//find position of current product id in above array
	$product_id_index = array_search($product_id, $product_ids);
	//split the array at the above index, putting the affiliate into position array[1]
	$product_with_label = explode(':', $product_ids[$product_id_index]);
	
	//check if this iteration of $product_with_label contains anything, if it does, it means it is the affiliate. 

		if (!empty($product_with_label[1])) {
			$affiliate = $product_with_label[1];
			//set cart item data for this product.
			$cart_item_data['affiliate'] = array(
				'label' => __('Affilate'),
				'value' => $affiliate,
			);
		}

    return $cart_item_data;
}

// Display item custom data in cart and checkout pages
add_filter( 'woocommerce_get_item_data', 'render_custom_data_on_cart_and_checkout', 20, 2 );
function render_custom_data_on_cart_and_checkout( $cart_data, $cart_item ){
    $custom_items = array();

    if( !empty( $cart_data ) )
        $custom_items = $cart_data;

    if( isset( $cart_item['affiliate'] ) )
        $custom_items[] = array(
            'name'  => $cart_item['affiliate']['label'],
            'value' => wc_clean($cart_item['affiliate']['value']),
        );

    return $custom_items;
}

function add_affiliate_to_order_items( $item, $cart_item_key, $values, $order ) {
	
	if ( empty( $values['affiliate'] ) ) {
        return;
    }
 
    $item->add_meta_data( __( 'Affiliate', 'ETS' ), wc_clean($values['affiliate']['value']) );
}
 
add_action( 'woocommerce_checkout_create_order_line_item', 'add_affiliate_to_order_items', 10, 4);

//If affiliate is FHT Member, apply fhtap10 coupon
add_action('woocommerce_before_calculate_totals', 'fht_member_coupon');

function fht_member_coupon($cart_object) {

	$is_fht_affiliate = false;
	$has_fhtap_course = false;

	foreach($cart_object->cart_contents as $cart_item) {
		if (!empty($cart_item['affiliate']) && $cart_item['affiliate'] && $cart_item['affiliate']['value'] == 'FHT Member') {
			$is_fht_affiliate = true;
		}

		if ($cart_item['product_id'] === 193) {
			$has_fhtap_course = true;
		} 
	}

	if ($is_fht_affiliate && $has_fhtap_course) {
		$applied_coupons  = WC()->cart->get_applied_coupons();
		$coupon_code = 'fhtap10';
		if (!in_array($coupon_code, $applied_coupons)) {
				WC()->cart->apply_coupon($coupon_code);
		}
	} else {
		$applied_coupons  = WC()->cart->get_applied_coupons();
		$coupon_code = 'fhtap10';
		if (in_array($coupon_code, $applied_coupons)) {
				WC()->cart->remove_coupon($coupon_code);
		}
		
	}
}

//show promotional product if AP CPD course
function promotion_ap_manual() {
	$cart_object = WC()->cart->get_cart();
	
	$cpd_products = [193,191,194,196];
	$promo_product_in_basket = false;
	$show_promo_popup = false;


	foreach($cart_object as $cart_item) {
		if ($cart_item['product_id'] === 201) {
			$promo_product_in_basket = true;
		}

		if (in_array($cart_item['product_id'], $cpd_products)) {
			$show_promo_popup = true;
		}
	}

	if ($promo_product_in_basket === false && $show_promo_popup === true) {
		return true; 
	}
}

add_action('wp_footer', 'display_promo_popup');

function display_promo_popup() {
	$display_promo_popup = promotion_ap_manual();

	if ($display_promo_popup) {
		promo_popup(201);
	}
}

//function for displaying promo_popup
function promo_popup($promo_product_id) { 
	$promo_product = wc_get_product($promo_product_id);
ob_start();
?>

<div class="promo__popup" style="display: none;">
		<div class="promo__toggle"><i class="fas fa-chevron-up"></i></div>
		<h3>Promotion!</h3>
		<p>Why not add <?php echo $promo_product->get_title(); ?> too?</p>
    <div class="card">
        <div class="card-image">
            <figure class="image related-courses__image">
                <a href="/basket/?add-to-cart=<?php echo $promo_product_id; ?>" title="<?php echo $promo_product->get_title(); ?>">
                    <img src="<?php echo get_the_post_thumbnail_url($promo_product_id, 'full') ?>" alt="<?php echo $promo_product->get_title(); ?>" class="product-box__accreditation">
                </a>
            </figure>
        </div>
        <div class="card-content">

            <div class="content">
                <p class="title--semibold title--small title--blue">
                    <a href="/basket/?add-to-cart=<?php echo $promo_product_id; ?>" title="<?php echo $promo_product->get_title(); ?>"><?php echo $promo_product->get_title(); ?></a>
                </p>

                <div class="level">
                    <div class="level-left">
                        <div class="level-item title--small title--blue">
													<span class="offers__regular-price"><?php echo $promo_product->get_price(); ?></span> <span class="offers__promo-price">£<?php echo get_special_offer_price($promo_product_id, 'college'); ?></span>
												 </div>
                    </div>

                    <div class="level-right">
                        <div class="level-item">
                            <div>
                                <a href="/basket/?add-to-cart=<?php echo $promo_product_id; ?>" class="btn btn--blue btn--hover-yellow">Add to basket</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
	return ob_end_flush();
}

//apply discount code for manual if AP CPD course in basket
add_action('woocommerce_before_calculate_totals', 'apply_ap_manual_discount', 15);

function apply_ap_manual_discount($cart_object) {
	$all_cart_item_ids = [];
	$contains_ap_cpd_course = false;
	$cpd_products = [193,191,194,196];


	foreach($cart_object->cart_contents as $cart_item) {
		array_push($all_cart_item_ids, $cart_item['product_id']);

		if ($contains_ap_cpd_course !== true) {
			if ( in_array($cart_item['product_id'], $cpd_products) ) {
				$contains_ap_cpd_course = true;
			} 
		}

	}

	if ( in_array(201, $all_cart_item_ids) && $contains_ap_cpd_course === true) {
		$applied_coupons  = WC()->cart->get_applied_coupons();
		$coupon_code = 'apmanualdiscount';
		if (!in_array($coupon_code, $applied_coupons)) {
				WC()->cart->apply_coupon($coupon_code);
				wc_clear_notices();
				wc_add_notice(__("Your promotional discount has been applied", "woocommerce"), "notice");
		}
	} else {
		return;
	}
}

function filter_wc_stripe_payment_metadata( $metadata, $order, $source ) {
		
	$count = 1;
	foreach( $order->get_items() as $item_id => $line_item ){
			$item_data = $line_item->get_data();
			$product = $line_item->get_product();
			$product_name = $product->get_name();
			$item_quantity = $line_item->get_quantity();
			$item_total = $line_item->get_total();
			$metadata['Line Item '.$count] = 'Product name: '.$product_name.' | Quantity: '.$item_quantity.' | Item total: '. number_format( $item_total, 2 );
			$count += 1;
	}
	return $metadata;
}
add_filter( 'wc_stripe_payment_metadata', 'filter_wc_stripe_payment_metadata', 10, 3 );

//Join Newsletter custom field on checkout

 /**
 * Add the field to the checkout
 */
add_action( 'woocommerce_after_checkout_billing_form', 'join_newsletter_cf' );

function join_newsletter_cf( $checkout ) {

	echo '<div class="join_newsletter_cf_wrap">';
	echo '<div><h3 class="title title--blue title--small" style="margin-top:30px;">Stay up to date with our latest news and offers</h3>';

	echo '<div class="join_newsletter_check">';

	woocommerce_form_field( 'join_newsletter_check', array(
        'type'          => 'checkbox',
        'class'         => array('join_newsletter_checkbox'),
		'label'         => 'Please subscribe me to your newsletter',
		'required'		=> false,
		'placeholder'   => '',
		), $checkout->get_value( 'join_newsletter_check' ));
		
	echo '</div>';
	echo '</div>';
	echo '</div> <!-- join_newsletter_check -->';
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'join_newsletter_cf_update_order_meta' );

function join_newsletter_cf_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['join_newsletter_check'] ) ) {
        update_post_meta( $order_id, 'join_newsletter', 'Join the newsletter' );
    }
    if ( ! empty( $_POST['join_newsletter_check'] ) ) {
        update_post_meta( $order_id, 'join_newsletter_check', sanitize_text_field( $_POST['join_newsletter_check'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'join_newsletter_cf_display_admin_order_meta', 10, 1 );

function join_newsletter_cf_display_admin_order_meta($order){

		if (get_post_meta( $order->get_id(), 'join_newsletter_check', true ) == 1) {
			echo '<p><strong>'.__('Subscribe me').':</strong> Yes </p>';
		}
}
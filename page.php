<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Essential_Training_Solutions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="section">
				<div class="container default-page__container">
					<div class="columns is-centered">
						<div class="column is-11-desktop is-full-tablet">
							<?php
							while ( have_posts() ) :
								the_post();

								if ($post->post_parent == 7) {
									get_template_part('template-parts/content', 'product-cat');
								} else {
									
									get_template_part( 'template-parts/content', 'page' );

								}
							
								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;

							endwhile; // End of the loop.
							?>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

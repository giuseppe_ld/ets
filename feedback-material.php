<?php 
/* Template Name: Feedback - Others */ 
get_header();
global $wpdb;
$sucess=0;
$name="";$easytouse="";$learning_needs="";$three_things_like="";$not_covered="";$study_material="";$format="";$omitted="";$valueformoney="";$postcode="";$when_you_purchased="";$email="";$exams_taken="";$exams_passed="";$prime_source_of_reference="";$significantly_contributed="";$source_of_refresher_training="";$time_spent="";$recommended="";$howtoimprove="";$comments="";

if(isset($_POST['new_hidden_2']))
{	
	$name=$_POST['name_1'];
	$postcode=$_POST['postcode_1'];
	$email=$_POST['email_1'];
	$study_material=$_POST['study_material'];
	$format=$_POST['format'];
	$learning_needs=$_POST['learning_needs'];
	$easytouse=$_POST['easytouse'];
	$three_things_like=$_POST['three_things_like'];
	$not_covered=$_POST['not_covered'];
	$omitted=$_POST['omitted'];
	$valueformoney=$_POST['valueformoney'];
	$when_you_purchased=$_POST['when_you_purchased'];
	$exams_taken=$_POST['exams_taken'];
	$exams_passed=$_POST['exams_passed'];
	$prime_source_of_reference=$_POST['prime_source_of_reference'];
	$significantly_contributed=$_POST['significantly_contributed'];
	$source_of_refresher_training=$_POST['source_of_refresher_training'];
	$time_spent=$_POST['time_spent'];
	$recommended=$_POST['recommended'];
	$howtoimprove=$_POST['howtoimprove'];
	$comments=$_POST['comments'];

	if($name == "" || $postcode == "" || $email == ""  || $study_material == "" || $format == "")
    		{
    			$sucess=2;
    		}
    		else
	    	{
	    		
	    		$wpdb->insert('feedbackdata_2',array('Name'=>$name,'Postcode'=>$postcode,'Email'=>$email,'Study_material'=>$study_material,'Format'=>$format, 'Learning_needs'=>$learning_needs,'Easytouse'=>$easytouse,'Three_things_like'=>$three_things_like,'Not_covered'=>$not_covered,'Omitted'=>$omitted,'Valueformoney'=>$valueformoney,'When_you_purchased'=>$when_you_purchased,'Exams_taken'=>$exams_taken,'Exams_passed'=>$exams_passed,'Prime_source_of_reference'=>$prime_source_of_reference,'Significantly_contributed'=>$significantly_contributed,'Source_of_refresher_training'=>$source_of_refresher_training,'Time_spent'=>$time_spent,'Recommended'=>$recommended,'Howtoimprove'=>$howtoimprove,'Comments'=>$comments),array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'));
	    		 $sucess=1;
	    		 $name="";$easetouse="";$learning_needs="";$three_things_like="";$not_covered="";$study_material="";$format="";$omitted="";$valueformonay="";$postcode="";$when_you_purchased="";$email="";$exams_taken="";$exams_passed="";$prime_source_of_reference="";$significantly_contributed="";$source_of_refresher_training="";$time_spent="";$recommended="";$howtoimprove="";$comments="";
	    	}

}

?>
<div id="primary" class="content-area">
	<main id="main" class="site-main section">
		<div class="container">
			<div class="columns is-centered">
				<div class="column is-11-widescreen is-10-desktop is-10-tablet">

			<?php if($sucess == 2){?><br><p align="center" style="color: red;">Fields marked with * are mandatory!</p><?php }elseif($sucess == 1){?><br><p align="center" style="color: green;">Thank you for the feedback!</p><?php }?>

		<div style="margin: 0 auto; padding-bottom: 20px;padding-top: 20px;">
			<form action="" method="post" id="feedbackform-material">
  
    

				<div class="form__group">
				  <label class="form-field__label">Your Name <span style="color: red;">*</span></label>
				  <input class="form-field__input" type="text" size="45" name="name_1" value="">
				</div>

				<div class="form__group" style="margin-top: 0;">
				<label class="form-field__label">Postcode <span style="color: red;">*</span></label>
				<input class="form-field__input" type="text" size="45" name="postcode_1" value="">
				</div>

				<div class="form__group">
				<label class="form-field__label">Email <span style="color: red;">*</span></label>
				<input class="form-field__input" name="email_1" type="email" size="45" value="">
				</div>
				<div class="form__group">
				  <label class="form-field__label--select">Study Material <span style="color: red;">*</span></label>
				<select name="study_material" id="study_material" style="width: 350px; height: 30px;">
				  <option value="0" >-Select -</option>
				  <option value="Anatomy &amp; Physiology">Anatomy &amp; Physiology</option>
				  <option value="A&amp;P Elementary">A&amp;P Elementary</option>
				  <option value="Aromatherapy">Aromatherapy</option>
				  <option value="Reflexology">Reflexology</option>
				</select>
				</div>

				<div class="form__group">
				<label class="form-field__label--select">Format <span style="color: red;">*</span></label>
				<select name="format" id="format" style="width: 350px; height: 30px;">
					<option value="0">- Select -</option>
					<option value="Online Resource">Online Resource</option>
					<option value="Question Bank">Question Bank</option>
					<option value="CD Rom">CD Rom</option>
					<option value="Study Manual">Study Manual</option>
					<option value="Combo Pack">Combo Pack</option>
				</select>
				</div>

				<div class="form__group">
				<label>Did this recource meet your own learning needs?</label> 
				<textarea name="learning_needs" cols="40" rows="2" ></textarea>
				</div>

				<div class="form__group">
				<label>Did you find the resource easy to use?</label>
				<textarea name="easytouse" cols="40" rows="2" ></textarea>
				</div>

				<div class="form__group">
				<label>List three things you liked about this resource</label>
				<textarea name="three_things_like" cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>Were there any aspects of the resource that you hoped to learn that were not covered?</label>
				<textarea name="not_covered" cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>Were there any elements of the resource that you think could have been omitted?</label>
				<textarea name="omitted" cols="40" rows="2" ></textarea>
				</div>

				<div class="form__group">
				<label>Was the resource good value for money?</label>
				<textarea name="valueformoney"  cols="40" rows="2" ></textarea>
				</div>

				<div class="form__group">
				<label>Were you a student when you purchased the resource?</label>
				<textarea name="when_you_purchased"  cols="40" rows="2" ></textarea>
				</div>

				<div class="form__group">
				<label>Have you taken your exams?</label>
				<textarea name="exams_taken"  cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>Did you pass your exams?</label> 
				<textarea name="exams_passed"  cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>Was this resource a prime source of reference during your studies?</label>
				<textarea name="prime_source_of_reference"  cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>Do you think this resource significantly contributed to your success?</label>
				<textarea name="significantly_contributed"  cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>Will you use this resource as a source of refresher-training post qualification?</label>
				<textarea name="source_of_refresher_training"  cols="40" rows="2" ></textarea>
				</div>

				<div class="form__group">
				<label>Approximately how many hours have you spent using this resource?</label>
				<textarea name="time_spent"  cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>Have you ever recommended products by Essential Training Solutions?</label>
				<textarea name="recommended"  cols="40" rows="2" ></textarea>
				</div>

				<div class="form__group">
				<label>What could we do to improve our products and / or services for you?</label>
				<textarea name="howtoimprove"  cols="40" rows="2"></textarea>
				</div>

				<div class="form__group">
				<label>If you have any comments that you would like to make, please do so here. </label>
				<textarea name="comments"  cols="40" rows="2" ></textarea>
				</div>

				<div><button type="submit" form="feedbackform-material" class="btn btn--blue btn--large btn--hover-yellow" value="Submit">Submit</button><input class="form-field__input" type="hidden" name="new_hidden_2" value="1"></div>
				</form>
		</div>

				</div>
			</div>
		</main>
</div>
<?php
get_footer();

?>
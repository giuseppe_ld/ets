<?php
/**
 * Essential Training Solutions functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Essential_Training_Solutions
 */

if ( ! function_exists( 'essential_training_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function essential_training_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Essential Training Solutions, use a find and replace
		 * to change 'essential-training' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'essential-training', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'essential-training' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'essential_training_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'essential_training_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function essential_training_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'essential_training_content_width', 640 );
}
add_action( 'after_setup_theme', 'essential_training_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function essential_training_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'essential-training' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'essential-training' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'essential_training_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function essential_training_scripts() {

	wp_enqueue_style( 'essential-training-woocommerce-style', get_template_directory_uri() . '/woocommerce.css' );

	wp_enqueue_style( 'essential-training-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Barlow:400,400i,500,600', false );

	wp_enqueue_script( 'essential-training-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
		
	wp_enqueue_script( 'essential-training-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/util/owl.min.js', array(), '20151215', true );
	
	wp_enqueue_script( 'bulma-carousel', get_template_directory_uri() . '/js/util/bulma-carousel.min.js', array(), '20151215', true );
	
	if (is_page('faqs') || is_page('ets-affiliate-information')) {
		wp_enqueue_script( 'bulma-accordion', get_template_directory_uri() . '/js/util/bulma-accordion.min.js', array(), '20151215', true );
	}

	wp_enqueue_script( 'essential-training-app', get_template_directory_uri() . '/js/app.js', array('jquery'), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'essential_training_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/feedback.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


/**
 * ETS Functions
 */

/** Set time zone */
date_default_timezone_set('Europe/London');

/** Print_r array to page with styling */
function code($arr) {
	echo "<pre><code>";
	print_r($arr);
	echo "</pre></code>";
}

function console($arr) { ?>
	
	<script type="text/javascript">
		var arr = <?php echo json_encode($arr); ?>;
		console.log(arr);
	</script>
<?php }

function console_acf() { 
	
$x = get_fields(); ?>

	<script type="text/javascript">
		var arr = <?php echo json_encode($x); ?>;
		console.log(arr);
	</script>
<?php }

/** Takes array, returns comma split list */
function array_to_list($arrs) {
	echo implode(', ', $arrs);
}

/** Takes array, returns splace split list */
function array_to_classes($arrs) {
	echo implode(' ', $arrs);
}

/** Add Page slug to body class */
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

function get_woo_categories() {
	$taxonomy     = 'product_cat';
	$orderby      = 'include';  
	$show_count   = 0;      // 1 for yes, 0 for no
	$pad_counts   = 0;      // 1 for yes, 0 for no
	$hierarchical = 1;      // 1 for yes, 0 for no  
	$title        = '';  
	$empty        = 0;
	$include	  = ['17', '18', '19', '21', '20']; //ids of woo cats in order required
  
	$args = array(
		   'taxonomy'     => $taxonomy,
		   'orderby'      => $orderby,
		   'show_count'   => $show_count,
		   'pad_counts'   => $pad_counts,
		   'hierarchical' => $hierarchical,
		   'title_li'     => $title,
		   'hide_empty'   => $empty,
		   'include' 	  => $include,
	);
   return get_terms( $args );
}

function get_woo_category_image($cat_id) {
	$thumbnail_id = get_term_meta($cat_id, 'thumbnail_id', true);
	return wp_get_attachment_url($thumbnail_id);
}


/** Set alt, title and description for image uploads */
function set_image_meta_upload( $post_ID ) {
    if ( wp_attachment_is_image( $post_ID ) ) {
    $my_image_title = get_post( $post_ID )->post_title;
    $my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',
    $my_image_title );
    $my_image_title = ucwords( strtolower( $my_image_title ) );
    $my_image_meta = array(
    'ID' => $post_ID,
    'post_title' => $my_image_title,
    'post_content' => $my_image_title,
    );

    // Set the image Alt-Text
    update_post_meta( $post_ID, '_wp_attachment_image_alt',
    $my_image_title );
    // Set the image meta (e.g. Title, Excerpt, Content)
    wp_update_post( $my_image_meta );
    }
}
add_action( 'add_attachment', 'set_image_meta_upload' );

//Add classes to nav menu anchors
function add_link_atts($atts) {
	$atts['class'] = 'navbar-item';
	return $atts;
  }
  add_filter( 'nav_menu_link_attributes', 'add_link_atts');


// Register Custom Post Types
add_action('init', 'register_custom_posts_init');
function register_custom_posts_init() {
    // Register Products
    $accreditors_labels = array(
        'name'               => 'Accreditors',
        'singular_name'      => 'Accreditor',
		'menu_name'          => 'Accreditors',
		'add_new_item'          => 'Add new Accreditor',
		'add_new'          => 'Add new Accreditor',
		
    );
    $accreditors_args = array(
        'labels'             => $accreditors_labels,
        'public'             => true,
        'capability_type'    => 'post',
		'has_archive'        => false,
		'exclude_from_search' => true,
		'menu_position'      => 20,
		'menu_icon'          => 'dashicons-star-filled',
        'supports'           => array( 'title', 'thumbnail', 'revisions', 'page-attributes' )
    );
    register_post_type('accreditors', $accreditors_args);

		// Register Affiliates
    $affiliates_labels = array(
        'name'               => 'Affiliates',
        'singular_name'      => 'Affiliate',
		'menu_name'          => 'Affiliates',
		'add_new_item'          => 'Add new Affiliate',
		'add_new'          => 'Add new Affiliate',
		
    );
    $affiliates_args = array(
        'labels'             => $affiliates_labels,
        'public'             => true,
        'capability_type'    => 'post',
		'has_archive'        => false,
		'exclude_from_search' => true,
		'menu_position'      => 20,
		'menu_icon'          => 'dashicons-groups',
        'supports'           => array( 'title', 'thumbnail', 'revisions', 'page-attributes' )
    );
    register_post_type('affiliate', $affiliates_args);
}

// Add the custom columns to the accreditors post type:
add_filter( 'manage_accreditors_posts_columns', 'set_custom_edit_accreditors_columns' );
function set_custom_edit_accreditors_columns($columns) {
	unset($columns['date']);
	$columns['thumbnail'] = 'Thumbnail';
	$columns['date'] = 'Date';

    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_accreditors_posts_custom_column' , 'custom_accreditors_column', 10, 2 );
function custom_accreditors_column( $column, $post_id ) {
    switch ( $column ) {

        case 'thumbnail' :
			$thumbnail = get_the_post_thumbnail($post_id, array('200','100'));
			echo  $thumbnail;
		break;

    }
}

// Add AFC Option page

if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' => 'General Content',
		'menu_title' => 'General Content',
		'icon_url' => 'dashicons-text',
	));
}

//Converting Accreditor to predefined number and return
function accreditors_as_numbers($accreditor) {
	// code($accreditor);
	$accreditation_field = get_field_object('field_5c3dddee828df'); 
	$accreditors = $accreditation_field['choices'];
	$accreditor_keys = array_keys($accreditation_field['choices']);
	$accreditor_removedkeys = array_shift($accreditor_keys);
	$key = array_search($accreditor, $accreditor_keys);
	// code($key);
	return $key;

}

//Conditions 72 Shortcode
function cond_a($atts, $content = null) {
	$a = shortcode_atts( array(
		'product-type' => 'course',
	), $atts );
	$items = get_field('conditions_a', 'options');
	$itemsArr = preg_split('/\r\n|\r|\n/', $items);
	$count = count($itemsArr);
	ob_start(); ?>

	<?php 
		if ($content) {
			echo "<p>";
			echo sprintf($content, $count, $a['product-type']);
			echo "</p>";
		}
	?>
	<div class="text-columns text-columns--3 product__long-list">
		<ul>
		<?php 
			foreach ($itemsArr as $item) {
				echo "<li>" . $item . "</li>";
			}
		?>
		</ul>
	</div>

	<?php
	return ob_get_clean();
}

//Conditions 80 Shortcode
function cond_b($atts, $content = null) {
	$a = shortcode_atts( array(
		'product-type' => 'course',
	), $atts );
	$items = get_field('conditions_b', 'options');
	$itemsArr = preg_split('/\r\n|\r|\n/', $items);
	$count = count($itemsArr);
	ob_start(); ?>

	<?php 
		if ($content) {
			echo "<p>";
			echo sprintf($content, $count, $a['product-type']);
			echo "</p>";
		}
	?>
	<div class="text-columns text-columns--3 product__long-list">
		<ul>
		<?php 
			foreach ($itemsArr as $item) {
				echo "<li>" . $item . "</li>";
			}
		?>
		</ul>
	</div>

	<?php
		return ob_get_clean();
	}

	//Conditions C Shortcode
function cond_c($atts, $content = null) {
	$a = shortcode_atts( array(
		'product-type' => 'course',
	), $atts );
	$items = get_field('conditions_c', 'options');
	$itemsArr = preg_split('/\r\n|\r|\n/', $items);
	$count = count($itemsArr);
	ob_start(); ?>

	<?php 
		if ($content) {
			echo "<p>";
			echo sprintf($content, $count, $a['product-type']);
			echo "</p>";
		}
	?>
	<div class="text-columns text-columns--3 product__long-list">
		<ul>
		<?php 
			foreach ($itemsArr as $item) {
				echo "<li>" . $item . "</li>";
			}
		?>
		</ul>
	</div>

	<?php
		return ob_get_clean();
	}
//Conditions D Shortcode
function cond_d($atts, $content = null) {
	$a = shortcode_atts( array(
		'product-type' => 'course',
	), $atts );
	$items = get_field('conditions_d', 'options');
	$itemsArr = preg_split('/\r\n|\r|\n/', $items);
	$count = count($itemsArr);
	ob_start(); ?>

	<?php 
		if ($content) {
			echo "<p>";
			echo sprintf($content, $count, $a['product-type']);
			echo "</p>";
		}
	?>
	<div class="text-columns text-columns--3 product__long-list">
		<ul>
		<?php 
			foreach ($itemsArr as $item) {
				echo "<li>" . $item . "</li>";
			}
		?>
		</ul>
	</div>

	<?php
		return ob_get_clean();
	}

	//Conditions all Shortcode
function cond_all($atts, $content = null) {
	$a = shortcode_atts( array(
		'product-type' => 'course',
	), $atts );
	$itemsA = get_field('conditions_a', 'options');
	$itemsB = get_field('conditions_b', 'options');
	$itemsAll = $itemsA . $itemsB;
	$itemsArr = preg_split('/\r\n|\r|\n/', $itemsAll);
	$count = count($itemsArr);
	ob_start(); ?>

	<?php 
		if ($content) {
			echo "<p>";
			echo sprintf($content, $count, $a['product-type']);
			echo "</p>";
		}
	?>
	<div class="text-columns text-columns--3 product__long-list">
		<ul>
		<?php 
			foreach ($itemsArr as $item) {
				echo "<li>" . $item . "</li>";
			}
		?>
		</ul>
	</div>

	<?php
		return ob_get_clean();
	}
//Oils shortcode	
function oils_a($atts, $content = null) {
	$a = shortcode_atts( array(
		'product-type' => 'manual',
	), $atts );
	$items = get_field('oils_a', 'options');
	$itemsArr = preg_split('/\r\n|\r|\n/', $items);
	$count = count($itemsArr);
	ob_start(); ?>

	<?php 
		if ($content) {
			echo "<p>";
			echo sprintf($content, $count, $a['product-type']);
			echo "</p>";
		}
	?>
	<div class="text-columns text-columns--3 product__long-list">
		<ul>
		<?php 
			foreach ($itemsArr as $item) {
				echo "<li>" . $item . "</li>";
			}
		?>
		</ul>
	</div>

	<?php
		return ob_get_clean();
	}
	
//shortcode button for posts

//Register shortcodes
function register_shortcodes(){
	add_shortcode('conditions-a', 'cond_a');
	add_shortcode('conditions-b', 'cond_b');
	add_shortcode('conditions-c', 'cond_c');
	add_shortcode('conditions-d', 'cond_d');
	add_shortcode('conditions-all', 'cond_all');
	add_shortcode('oils-a', 'oils_a');
 }

 //init shortcodes
 add_action( 'init', 'register_shortcodes');

 //Don't wrap shortcodes in p
 function wpex_clean_shortcodes($content){   
	$array = array (
		'<p>[' => '[', 
		']</p>' => ']', 
		']<br />' => ']'
	);
	$content = strtr($content, $array);
	return $content;
	}
	
add_filter('the_content', 'wpex_clean_shortcodes');

//Add admin css
function admin_style() {
	wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
  }
add_action('admin_enqueue_scripts', 'admin_style');

function get_product_feedback($id) {
	global $wpdb;
	$results = $wpdb->get_results(
		"SELECT * FROM feedbackdata
		WHERE courseID = $id AND custapprove = 'yes'
		ORDER BY date DESC
		LIMIT 3 "); 
	$results_array = json_decode(json_encode($results), true);
	return $results_array;
}

function get_feedback_stars($score, $unqid) {

	$rating = (5/40) * $score;
	$ratingRounded = round($rating);
	$whole = floor($rating);
	$fraction = ($rating - $whole) * 100;

	for ($i = 0; $i < $ratingRounded; $i++) {
		if ($i == ($ratingRounded - 1) && $fraction > 25) {
			echo '<i class="fas fa-star testimonial-card__meta-rating-star split-star" data-splitstar="' . $fraction . '" data-star="' . $unqid . '"></i>';
		} else {
			echo '<i class="fas fa-star testimonial-card__meta-rating-star"></i>';
		}

	}
}


function get_overall_product_feedback($id) {
	global $wpdb;
	$results = $wpdb->get_results(
		"SELECT * FROM product_feedback
		WHERE product_id = $id
		", ARRAY_A
	); 

	if($results[0]['total_score'] > 0) {
		$unqid = $results[0]['product_id'];
		$score = (int) $results[0]['total_score'];
		$count = (int) $results[0]['product_count'];
	
		$rating = (5/(40*$count)) * $score;
		$ratingRounded = round($rating);
		$whole = floor($rating);
		$fraction = ($rating - $whole) * 100;
	
		for ($i = 0; $i < $ratingRounded; $i++) {
			if ($i == ($ratingRounded - 1) && $fraction > 25) {
				echo '<i class="fas fa-star testimonial-card__meta-rating-star split-star" data-splitstar="' . $fraction . '" data-star="' . $unqid . '"></i>';
			} else {
				echo '<i class="fas fa-star testimonial-card__meta-rating-star"></i>';
			}
	
		}
	}
}

function get_featured_product_feedback() {
	global $wpdb;
	$results = $wpdb->get_results(
		"SELECT * FROM feedbackdata
		WHERE etsapprove = 'yes' AND custapprove = 'yes'
		ORDER BY date DESC
		LIMIT 6", ARRAY_A); 
	
	return $results;
}



//Cron function to update product feedback once a day, used on product cat pages

add_action( 'ets_cache_product_feedback', 'ets_cache_product_feedback_func' );
 
function ets_cache_product_feedback_func() {
	global $post;
	global $wpdb;
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => '-1',
		'orderby' => 'id',
		'order' => 'ASC',
	);
	
	$wpdb->query("TRUNCATE TABLE product_feedback");

	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 

		$total_score = $wpdb->get_results(
			"SELECT SUM(totalscore) 
			AS total_score
			FROM feedbackdata
			WHERE courseID = $post->ID
		", ARRAY_A);

		$total_score_result = $total_score[0]['total_score'] ? $total_score[0]['total_score'] : 0 ;
		// code($total_score_result);

		$count = $wpdb->get_results(
			"SELECT COUNT(*) 
			AS count
			FROM feedbackdata
			WHERE courseID = $post->ID
		", ARRAY_A);

		$count_result = $count[0]['count'] ? $count[0]['count'] : 0 ;
		// code($count_result);
		

		$wpdb->replace(
			'product_feedback',
			array(
			'product_id' => $post->ID,
			'total_score' => $total_score_result,
			'product_count' => $count_result
			)
		);
	endwhile;
	/* Restore original query Data */
	wp_reset_query();

}

add_filter('wpcf7_autop_or_not', '__return_false');

//return list of ids for all special offer products
function get_special_offer_products() {

	$special_offer_products = [];

	$args = array(
		'post_type' => 'product',
		'posts_per_page' => '-1',
		'orderby' => 'id',
		'order' => 'ASC',
		'tax_query' =>	array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'product_cat',
				'field' => 'slug',
				'terms' => 'special-offers',
			)
		)
	);

	$loop = new WP_Query( $args );

	if ($loop->have_posts() > 0) :
		while ( $loop->have_posts() ) : $loop->the_post(); 
			array_push($special_offer_products, get_the_ID());
		endwhile;
		wp_reset_query();
	endif;

	return $special_offer_products;
	
}

//get special offer prices 
function get_special_offer_price($product_id, $discount_type) {
	if ($discount_type == 'college') {
		if (get_field('discount_value', $product_id) === true) {
			return get_field('discounted_price', $product_id);
		}	
	} else if ($discount_type == 'bulk') {
		if (get_field('bulk_discount_value', $product_id) === true) {
			return get_field('bulk_discounted_price', $product_id);
		}
	}
}
//amend prices on cart for special offer products
add_action( 'woocommerce_before_calculate_totals', 'add_custom_price', 20, 1 );

function add_custom_price( $cart_object ) {

		
	if ( is_admin() && ! defined( 'DOING_AJAX' ) )
	return;

	if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
	return;

	if(!empty($_REQUEST['discount'])) {
		$add_to_cart_string = $_REQUEST['add-to-cart'];
		$discounted_product_id = (explode('&',$add_to_cart_string));
		setcookie('discount-' . $_REQUEST['discount'], $discounted_product_id[0], time()+60, '/');
		if ($_REQUEST['discount'] === 'bulk') {
			$_COOKIE['discount-bulk'] = $discounted_product_id[0];
		} elseif ($_REQUEST['discount'] === 'college') {
			$_COOKIE['discount-college'] = $discounted_product_id[0];
		}
	}

	if (!empty($_COOKIE['discount-bulk'])) :
		$discount_bulk_id = $_COOKIE['discount-bulk'];
	endif;
	
	if (!empty($_COOKIE['discount-college'])) :
		$discount_college_id = $_COOKIE['discount-college'];
	endif;

	if( !empty($_COOKIE['discount-college'])) :
		$special_offer_products = get_special_offer_products();
		foreach ($cart_object->cart_contents as $value) {
			$special_offer_price = get_special_offer_price($value['product_id'], 'college');
			if (in_array($value['product_id'], $special_offer_products) && $value['product_id'] == $discount_college_id) {
				if ($value['product_id'] === 201 || $value['product_id'] === 226) {

					$applied_coupons  = WC()->cart->get_applied_coupons();
					$coupon_code = ($value['product_id'] === 201) ? 'APSTUDENT' : 'PATHSTUDENT';
					if (!in_array($coupon_code, $applied_coupons)) {
							WC()->cart->apply_coupon($coupon_code);
							wc_clear_notices();
							wc_add_notice(__("Your College Discount has been applied", "woocommerce"), "notice");
					}
					if (strpos($value['data']->get_name(), 'College discount') === false) {
						$new_product_name = $value['data']->get_name() . ' - College discount';

						$value['data']->set_name($new_product_name);
					}
				} else {
					$value['data']->set_price($special_offer_price);

					if (strpos($value['data']->get_name(), 'College discount') === false) {
							$new_product_name = $value['data']->get_name() . ' - College discount';

							$value['data']->set_name($new_product_name);
					}
				}
			}
		}	
	endif;
	
	if ( !empty($_COOKIE['discount-bulk'])) :
			$special_offer_products = get_special_offer_products();
			foreach ( $cart_object->cart_contents as $value ) {
				if ($value['quantity'] == get_field('bulk_amount', $value['product_id'])) :
					$special_offer_price = get_special_offer_price($value['product_id'], 'bulk');
					if ( in_array($value['product_id'], $special_offer_products) && $value['product_id'] == $discount_bulk_id) {
							$value['data']->set_price($special_offer_price);

							if (strpos($value['data']->get_name(), 'Bulk discount') === false) {
								$new_product_name = $value['data']->get_name() . ' - Bulk discount'; 

								$value['data']->set_name($new_product_name);
							}
						}
					endif;
				}
		endif;
	}



function str_spaces_to_dashes($string) {
	return str_replace(' ', '-', $string);
}

function str_to_lowercase($string) {
	return strtolower($string);
}
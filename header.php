<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Essential_Training_Solutions
 */

if (get_field('product_type') === 'revision' || get_field('product_type') === 'manual') {
	$alt_header = get_field('alt_header');
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	 crossorigin="anonymous">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site <?php if (get_field('header_type')) echo 'no_header' ?>">
		<a class="skip-link screen-reader-text" href="#content">
			<?php esc_html_e( 'Skip to content', 'essential-training' ); ?></a>

		<div class="topbar">
			<div class="container">
				<div class="level is-mobile">
					<div class="level-left">
						<div class="level-item">
							<div class="icon-text icon-text--white">
								<i class="fas fa-phone"></i> <a href="tel:<?php the_field('gen_phone_number', 'option'); ?>"><?php the_field('gen_phone_number', 'option'); ?></a>
							</div>
						</div>
						<div class="level-item is-hidden-mobile">
							<div class="icon-text icon-text--white">
								 <a href="mailto:<?php the_field('gen_email', 'option'); ?>" title="Email Essential Training Solutions"><i class="fas fa-envelope"></i> <?php the_field('gen_email', 'option'); ?></a>
							</div>
						</div>
					</div>

					<div class="level-right">
						<div class="level-item">
							<button class="btn btn--white btn--hover-outline-white btn-student" data-href="https://secure.webcampus.co.uk/ets/">Student Login</button>
						</div>
					</div>
				</div> <!-- .columns -->
			</div> <!-- .container -->

		</div> <!-- .topbar -->
		<?php if (is_front_page()) : ?>
			<header class="hero is-medium has-carousel">
			<div class="hero-carousel carousel-animated carousel-animate-fade" data-autoplay="true">
		<?php elseif (is_product()) : ?>
			<header class="hero is-medium has-carousel">
			<div class="hero-carousel">
		<?php else : ?>
			<header class="hero is-small has-carousel">
			<div class="hero-carousel">
		<?php endif; ?>
				<div class="carousel-container">
					<?php if (is_front_page()) : ?>
						<?php 
						$slides = get_field('slider_home');
						$i = 0;
						foreach ($slides as $slide) : ?>
						
							<?php if ($i == 0) : ?>
								<div class="carousel-item has-background is-active">
									<img class="is-background" src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>" />
								</div>
							<?php else : ?>
								<div class="carousel-item has-background">
									<img class="is-background" src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>" />
								</div>
							<?php endif; ?>
							<?php $i++; ?>
						<?php endforeach; ?>
					<?php elseif (get_field('product_type') === 'revision' || get_field('product_type') === 'manual') : 
					 ?>	
						<div class="carousel-item has-background is-active">
							<img class="is-background" src="<?php echo $alt_header; ?>" alt="<?php echo the_title(); ?>" />
						</div>
						<div class="carousel-item has-background">
							<img class="is-background" src="<?php echo $alt_header; ?>" alt="<?php echo the_title(); ?>" />
						</div>
					<?php elseif(has_post_thumbnail()) : ?>
						<div class="carousel-item has-background is-active">
							<img class="is-background" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>" />
						</div>
						<div class="carousel-item has-background">
							<img class="is-background" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>" />
						</div>
					<?php else : ?>
						<div class="carousel-item has-background is-active">
							<img class="is-background" src="/wp-content/uploads/header-slide-3.png" />
						</div>
						<div class="carousel-item has-background">
							<img class="is-background" src="/wp-content/uploads/header-slide-3.png" />
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="hero-head">
				<nav class="navbar is-transparent">
					<div class="container">
						<div class="navbar-brand">
							<a href="/">
								<img src="/wp-content/uploads/ets-logo.png" class="navbar-brand__logo" alt="Logo">
							</a>
							<span class="navbar-burger burger" data-target="navbarMenuHeroA">
								<span></span>
								<span></span>
								<span></span>
							</span>
						</div>
						<div id="navbarMenuHeroA" class="navbar-menu">
							<div class="navbar-end">
								<nav id="site-navigation" class="main-navigation">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',

										) );
									?>
								</nav><!-- #site-navigation -->
								
							</div>
						</div>
					</div>
				</nav>
			</div>
			<?php if (is_front_page()) : ?>
				<div class="hero-body has-text-centered" data-header="front-page">
					<div class="columns is-centered">
						<div class="column is-half is-5-fullhd">
							<div class="field has-addons">
								<div class="control is-expanded">
									<h1 class="hero-body__heading title has-text-centered"><?php the_field('page_heading'); ?></h1>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php elseif(is_404()) : ?>
		
		
			<?php elseif(is_search()) : ?>
			<div class="hero-body has-text-centered" data-header="search">
				<div class="container">
					<div class="columns is-mobile">
						<div class="column is-narrow">
							<div class="field has-addons">
								<div class="control is-expanded">
									<?php $page_heading = get_field('page_heading'); ?>
									<h3 class="hero-body__heading title has-text-centered">
										Search results for:<?php echo get_search_query(); ?>
									</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php elseif (is_product()) : ?>
			<div class="hero-body has-text-centered" data-header="product">
				<div class="container">
					<div class="columns is-centered is-vcentered">
						<div class="column is-11-widescreen is-full-tablet">
							<div class="columns is-vcentered">
								<div class="column is-9 is-narrow">
									<div class="field has-addons">
										<div class="control is-expanded">
											<?php $page_heading = get_field('page_heading'); ?>
											<h1 class="hero-body__heading title title--med title--normal-case no-mb"><?php echo the_title(); ?></h1>
											<?php
											if (get_field('product_type') == "course") :
												$course_info = get_field('course_group'); ?>
												<h3 class="hero-body__heading product__course-title title--normal-case"><?php echo $course_info['qualification_title']; if ($course_info['cert_end_date']) : ?> | Certification End Date : <?php echo $course_info['cert_end_date']; endif; ?></h3>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<?php
								if (get_field('product_type') == "course") : ?>
								<div class="column is-offset-1 is-2">
									<div class="accreditor">
	
										<p class="accreditor__info">ACCREDITED BY</p>
										<div class="accreditor__logo">
											<img src="<?php echo ets_display_accreditor($course_info['accreditation']); ?>" alt="<?php echo $course_info['qualification_title']; ?>" />
										</div>
									</div>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<?php elseif (is_page('vtct-registration')) : ?>
			<div class="hero-body has-text-centered" data-header="vtct-reg">
				<div class="container">
					<div class="columns is-centered is-vcentered">
						<div class="column is-11-widescreen is-full-tablet">
							<div class="level">
								<div class="level-left">
									<div class="level-item">
										<div>
										<?php $page_heading = get_field('page_heading'); ?>
											<h1 class="hero-body__heading title title--normal-case has-text-left" style="margin-bottom: 0;">
												<?php if ($page_heading) {
													echo $page_heading;
												} else {
													echo the_title();
												}?>
											</h1>
											<p>We require the following details so we can register you with the VTCT.</p>
										</div>
									</div>
								</div>
								<div class="level-right">
									<div class="level-item">
										<div>
											<img src="/wp-content/uploads/vtct-reg-logo.jpg" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<?php elseif (is_home()) : ?>
				<div class="hero-body has-text-centered" data-header="blog">
					<div class="container">
						<div class="columns is-centered has-text-centered is-vcentered">
							<div class="column is-11-widescreen is-full-tablet">
								<?php $page_heading = get_field('page_heading'); ?>
									<h1 class="hero-body__heading title title--normal-case has-text-centered" style="margin-bottom: 0;">
										Blog
									</h1>
							</div>
						</div>
					</div>
				</div>
		
			<?php elseif (is_single()) : ?>

			<?php else : ?>
			<div class="hero-body has-text-centered" data-header="all">
				<div class="container">
					<div class="columns is-centered is-vcentered">
						<div class="column is-11-widescreen is-full-tablet">
							<div class="columns">
								<div class="column is-narrow is-centered-mobile is-paddingless">
									<div class="field has-addons">
										<div class="control is-expanded">
											<?php $page_heading = get_field('page_heading'); ?>
											<h1 class="hero-body__heading title has-text-centered title--normal-case">
												<?php if ($page_heading) {
													echo $page_heading;
												} else {
													echo the_title();
												}?>
											</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</header>
		
		<div id="content" class="site-content">
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Essential_Training_Solutions
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">
		<div class="container">
			<div class="columns is-marginless is-centered">
				<div class="column is-10-mobile is-centered-mobile is-12-tablet is-11-desktop">

					<div class="columns justify-sb">
							
						<div class="column is-narrow footer-menu">
							<h4 class="footer-menu__heading has-text-centered-mobile">Keep in touch</h4>
							<div class="columns is-flex is-justify-center-mobile is-mobile">
								<div class="column is-1-tablet is-narrow-mobile"><i class="fas fa-envelope icon--yellow"></i></div>
								<div class="column is-narrow-mobile is-paddingless-lr-mobile"><a href="mailto:<?php the_field('gen_email', 'option'); ?>"><?php the_field('gen_email', 'option'); ?></a></div>
							</div>
							<div class="columns is-flex is-justify-center-mobile is-mobile">
								<div class="column is-1-tablet is-narrow-mobile"><i class="fas fa-phone icon--yellow"></i></div>
								<div class="column is-narrow-mobile is-paddingless-lr-mobile"><?php the_field('gen_phone_number', 'option'); ?></div>
							</div>
							<div class="columns is-flex is-justify-center-mobile is-mobile">
								<div class="column is-1-tablet is-narrow-mobile"><i class="fab fa-twitter icon--yellow"></i></div>
								<div class="column is-narrow-mobile is-paddingless-lr-mobile "><a href="https://twitter.com/PasswithETS">Follow us on twitter</a></div>
							</div>
							<div class="columns is-flex is-justify-center-mobile is-mobile">
								<div class="column is-1-tablet is-narrow-mobile"><i class="fab fa-facebook-f icon--yellow"></i></div>
								<div class="column is-narrow-mobile is-paddingless-lr-mobile"><a href="https://www.facebook.com/pages/Essential-Training-Solutions-Ltd/132014430165329">Like us on facebook</a></div>
							</div>
						</div>
		
						<div class="column is-narrow footer-menu__course-cats">
							<h4 class="footer-menu__heading has-text-centered-mobile">Our Courses</h4>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/courses/anatomy-and-physiology/">Anatomy and Physiology</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/courses/pathology/">Pathology</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/courses/health-and-safety/">Health and Safety</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/courses/aromatherapy/">Aromatherapy</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/courses/reflexology/">Reflexology</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/freebies">Freebies</a></div>
							</div>
						</div>
						
						<div class="column is-narrow footer-menu__course-cats">
							<h4 class="footer-menu__heading has-text-centered-mobile">Useful Links</h4>
							<!-- <div class="columns has-text-centered-mobile">
								<div class="column"><a href="/blog">Blog</a></div>
							</div> -->
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/colleges">Colleges</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/affiliates">Affiliates</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/ets-affiliate-information">Become an Affiliate</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/faqs">FAQs</a></div>
							</div>
							<div class="columns has-text-centered-mobile">
								<div class="column"><a href="/contact-us">Contact Us</a></div>
							</div>
						</div>
		
					</div>

				</div>
			</div>
			
			<div class="site-info">
				<div class="section">
						<div class="columns">
							<div class="column is-narrow-mobile has-text-centered-mobile">
							&copy; Essential Training Solutions <?php echo date("Y"); ?>
							</div>
							
							<div class="column is-narrow-mobile has-text-right has-text-centered-mobile">
								<a href="privacy-policy" title="Privacy Policy">Privacy Policy</a>
								<span class="sep">|</span> <a href="/terms-conditions" title="Terms & Conditions">Terms & Conditions</a>
								<span class="sep">|</span> <a href="/cookie-policy" title="Cookie Policy">Cookie Policy</a>
							</div>
						</div>
				</div>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

<?php
	$args = array(
		'post_type' => 'accreditors',
		'numberposts' => '-1',
	);
	$posts = get_posts($args);

?>
<div class="homepage-logos owl-carousel owl-theme">

		<?php foreach($posts as $post) : ?>
			<div>
				<figure class="image accreditation-image">
					<img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="<?php echo $post->post_title; ?>">
				</figure>
			</div>
		<?php endforeach; ?>

</div>
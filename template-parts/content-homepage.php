<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Essential_Training_Solutions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('content-space--top'); ?>>

	<section id="course-categories" class="content-space--bottom"> 

			<header class="entry-header">
				<h3 class="section__title title--blue title has-text-centered">Choose a course category</h3>
			</header><!-- .entry-header -->
			<div class="section">
				<div class="container">
					<div class="course-categories is-flex-mobile columns is-centered">
						<div class="column is-9 is-10-mobile is-10-tablet is-centered">
							<div class="section">
								<div class="columns is-multiline is-centered">
									
									<?php
										$cats = get_woo_categories();
										foreach($cats as $cat) : ?>
											
											<div class="course-category column is-4-desktop is-6-tablet">
												<a href="/courses/<?php echo $cat->slug; ?>" title="<?php echo $cat->name;?> Courses" class="card-link">
													<div class="card">
														<div class="card-image">
															<figure class="image course-category__image">
																<img src="<?php echo get_woo_category_image($cat->term_id); ?>" alt="<?php echo $cat->name;?> Courses">
															</figure>
														</div>

														<div class="card-content">
															<div class="content has-text-centered">
																<?php echo $cat->name;?>
															</div>
														</div>
													</div> <!-- .card -->
												</a> <!-- .card-link -->
											</div> <!-- .course-category -->

										<?php endforeach; ?>
									

									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>

	<section id="accreditations" class="content-space--top content-space--bottom section--blue section--bg"> 
		<div class="section">
			<header class="entry-header">
				<h3 class="section__title title--white title has-text-centered">Our courses are recognised by</h3>
			</header> <!-- .entry-header -->
			
			<?php get_template_part( 'template-parts/section', 'accreditations' ); ?>

		</div>
	</section>

	<section id="popular-courses" class="content-space--top content-space--bottom"> 
		<div class="section">
			<header class="entry-header">
				<h3 class="section__title title--blue title has-text-centered">Our most popular courses</h3>
			</header> <!-- .entry-header -->
			
			<?php get_template_part( 'template-parts/section', 'popular-courses' ); ?>
			
		</div>
	</section>

	<section id="testimonials" class="content-space--top section--blue section--bg">
		<header class="entry-header">
			<h3 class="section__title title--white title has-text-centered">Testimonials from past students</h3>
		</header> <!-- .entry-header -->
		
		<div class="section">
			<?php get_template_part('template-parts/section', 'testimonials-carousel'); ?>
		</div>
	</section>
	  
	
</article><!-- #post-<?php the_ID(); ?> -->

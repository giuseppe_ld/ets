<div class="vtct-reg__group vtct-reg--grey">
	<h3 class="vtct-reg__title title">Previous Registration Details</h3>
	<p class="vtct-reg__intro">If you <strong><em>already</em></strong> have any of the following registration numbers, please enter them below, else leave blank:</p>
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Unique Learner Number:</label>
		<div class="vtct-reg__inline-fields">
			[text uln1 maxlength:1 class:vtct-reg__uln][text uln2 maxlength:1 class:vtct-reg__uln][text uln3 maxlength:1 class:vtct-reg__uln][text uln4 maxlength:1 class:vtct-reg__uln][text uln5 maxlength:1 class:vtct-reg__uln][text uln6 maxlength:1 class:vtct-reg__uln][text uln7 maxlength:1 class:vtct-reg__uln][text uln8 maxlength:1 class:vtct-reg__uln][text uln9 maxlength:1 class:vtct-reg__uln][text uln10 maxlength:1 class:vtct-reg__uln]
		</div>
	</div>
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Scottish Candidate Number:</label>
		<div class="vtct-reg__inline-fields">[text scn1 maxlength:1 class:vtct-reg__scn][text scn2 maxlength:1 class:vtct-reg__scn][text scn3 maxlength:1 class:vtct-reg__scn][text scn4 maxlength:1 class:vtct-reg__scn][text scn5 maxlength:1 class:vtct-reg__scn][text scn6 maxlength:1 class:vtct-reg__scn][text scn7 maxlength:1 class:vtct-reg__scn][text scn8 maxlength:1 class:vtct-reg__scn][text scn9 maxlength:1 class:vtct-reg__scn]
		</div>
	</div>
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">VTCT Registration Number:</label>
		<div class="vtct-reg__inline-fields">[text vrn1 maxlength:1 class:vtct-reg__vrn][text vrn2 maxlength:1 class:vtct-reg__vrn][text vrn3 maxlength:1 class:vtct-reg__vrn][text vrn4 maxlength:1 class:vtct-reg__vrn][text vrn5 maxlength:1 class:vtct-reg__vrn][text vrn6 maxlength:1 class:vtct-reg__vrn][text vrn7 maxlength:1 class:vtct-reg__vrn][text vrn8 maxlength:1 class:vtct-reg__vrn][text vrn9 maxlength:1 class:vtct-reg__vrn]
		</div>
	</div>
</div>
<div class="vtct-reg__group vtct-reg--cream">
	<div class="level">
		<div class="level-left">
			<div class="level-item">
				<div><h3 class="vtct-reg__title title">Your Personal Details</h3></div>
			</div>
		</div>
		<div class="level-right">
			<div class="level-item">
				<div><span class="vtct-reg__warning">* compulsory field - please complete</span></div>
			</div>
		</div>
	</div>
	<p class="vtct-reg__intro"><strong>IMPORTANT: Candidates MUST use their legal name, not favoured names or nicknames.<br />
	Please be accurate - the VTCT will charge up to &pound; for any changes that have to be made after registration.</strong>
	</p>
	<div class="vtct-reg__fields">
			<label class="vtct-reg__label">Title:<span class="vtct-reg__warning">*</span></label>
			[text title class:vtct-reg__input placeholder "Eg. Mr, Mrs, Miss, Ms, Dr etc"]
	</div>	
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Forenames:<span class="vtct-reg__warning">*</span></label>
		[text* forenames class:vtct-reg__input]
	</div>	
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Surname:<span class="vtct-reg__warning">*</span></label>
		[text* Surname class:vtct-reg__input]
	</div>	
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Previous Surname (if any):</label>
		[text* previous_surname class:vtct-reg__input]
	</div>
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Date of Birth:<span class="vtct-reg__warning">*</span></label>
		[date* dob min:1929-01-01 max:2003-01-01 class:vtct-reg__date placeholder "DD/MM/YYYY"]
	</div>
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Gender:<span class="vtct-reg__warning">*</span></label>
		[select* gender class:vtct-reg__select first_as_label "Select your gender" "Male" "Female"]
	</div>
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Address:<span class="vtct-reg__warning">*</span></label>
		[textarea* address class:vtct-reg__textarea]
	</div>
	<div class="vtct-reg__fields">
		<label class="vtct-reg__label">Postcode:</label>
		[text* postcode class:vtct-reg__input]
	</div>
</div>
<div class="vtct-reg__group vtct-reg--cream">
	<h3 class="vtct-reg__title title">Reasonable Adjustments</h3>
	<p class="vtct-reg__intro">A reasonable adjustment is any action that helps to reduce the effect of a disability or difficulty that places the candidate at a substantial disadvantage in the assessment situation. If you feel that you have a learning difficulty/disability/health problem for which reasonable adjustments will be required on the final assessment please complete the area below.</p>
	<p class="vtct-reg__intro">If you do require Reasonable Adjustments, please state the Nature of Declaration, else leave blank:</p>
	<div class="vtct-reg__fields">
		[textarea reasonable-adjustments class:vtct-reg__textarea]
	</div>
</div>

<div class="vtct-reg__group vtct-reg__button">
	[submit class:vtct-reg__btn class:btn class:btn--blue class:btn--hover-outline-blue "Send details"]
</div>
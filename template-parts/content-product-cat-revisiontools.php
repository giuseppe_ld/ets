<?php 
$args = array(
	'post_type' => 'product',
	'posts_per_page' => '-1',
	'orderby' => 'id',
	'order' => 'ASC',
	'tax_query' =>	array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => $post->post_name,
		),
		array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => 'revision-tools',
		)
	)
);

$loop = new WP_Query( $args );
if ($loop->have_posts() > 0) :?>

<h2 class="product-section__title">Revision Tools</h2>

<?php
endif;
while ( $loop->have_posts() ) : $loop->the_post(); 

?>

<article class="media product-box product-box--revisiontools product-<?php echo $post->ID; ?>">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<?php if (get_the_post_thumbnail_url()) : ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
				</a>
			<?php else : ?>
				<img src="https://bulma.io/images/placeholders/128x128.png">
			<?php endif; ?>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">
			<h3 class="product-box__title"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
			<p class="product-box__desc">
				<?php echo get_the_excerpt(); ?>
			</p>

			<div class="level">
				<div class="level-left">
					<div class="level-item">
						<span class="product-box__price"><?php get_product_price($post->ID); ?></span>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/basket/?add-to-cart=<?php echo $post->ID; ?>" class="btn btn--outline-blue btn--hover-yellow">Order Now</a>
					</div>
					<div class="level-item">
						<a href="<?php the_permalink(); ?>" class="btn btn--blue btn--hover-yellow">More info</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>

<?php 
endwhile;
wp_reset_query();
?>



<?php  
	$mission = get_field('mission_statement');
	$about_ets = get_field('about_ets');
	$management = get_field('management_team');
?>

<section id="mission-statement" class="content-space--bottom content-space--top">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-9-widescreen is-10-desktop is-10-tablet">
				<div class="columns">
					<?php if($mission['image']['url']) :?>
					<div class="column is-6">
						<h3 class="section__title title title--small">
							<?php echo $mission['title']; ?>
						</h3>
						<?php echo $mission['content']; ?>
					</div>
					<div class="column is-6 bg--cover" style="background-image:url(<?php echo $mission['image']['url']; ?>);">
					</div>
					<?php else :?>
					<div class="column is-full has-text-centered">
						<h3 class="section__title title title--small">
							<?php echo $mission['title']; ?>
						</h3>
						<?php echo $mission['content']; ?>
					</div>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="about-ets" class="content-space--top content-space--bottom section--yellow section--bg">
	<div class="container">
		<header class="entry-header">
			<h3 class="section__title title title--small has-text-centered"><?php echo $about_ets['title']; ?></h3>
		</header> <!-- .entry-header -->
		
		<div class="columns is-centered">
			<div class="column is-9-widescreen is-10-desktop is-10-tablet">
				<div class="text-columns text-columns--2">
					<?php echo $about_ets['content']; ?>
				</div>
			</div>
		</div>

	</div>
</section>


<section id="management-team" class="content-space--top content-space--bottom">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-9-widescreen is-10-desktop is-10-tablet">
				<header class="entry-header">
					<h3 class="section__title title title--small"><?php echo $management['title']; ?></h3>
						
					<?php echo $management['content']; ?>
				</header>

		
				<div class="columns is-centered-mobile is-multiline is-centered">
					<?php foreach ($management['team_members'] as $team_member) : ?>
					<div class="column is-full-mobile is-full-tablet">
						<div class="employee">
							<div class="columns is-multiline">
								<div class="column is-5 employee__photo bg--cover" style="background-image:url('<?php echo $team_member['headshot']['url']; ?>');"></div>
								<div class="column is-7">
									<h4 class="employee__name"><?php echo $team_member['name']; ?></h4>
									<p class="employee__jobtitle"><?php echo $team_member['job_title']; ?></p>
									<div class="employee__bio"><?php echo $team_member['bio']; ?></div>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="testimonials" class="content-space--top section--blue section--bg">
	<div class="container">
		<header class="entry-header">
			<h3 class="section__title title--white title has-text-centered">Testimonials from past students</h3>
		</header> <!-- .entry-header -->

		<?php get_template_part('template-parts/section', 'testimonials-carousel'); ?>
	</div>
</section>
	
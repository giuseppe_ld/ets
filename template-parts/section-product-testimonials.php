<?php
$all_feedback = get_product_feedback($post->ID);

if (empty($all_feedback)) return;
?>
<div class="card product-side product-side__reviews">
	<h4 class="product-side__title">Reviews</h4>

	<div class="product-side__reviews__carousel">
		<div class="columns is-centered">
			<div class="column is-full is-paddingless">

				<div class='carousel carousel-animated carousel-animate-slide' data-size="1">
					<div class='carousel-container'>
						<?php foreach ($all_feedback as $feedback) : ?> 
						<?php
							$feedback_date = strtotime($feedback['date']);
							$feedback_date_formatted = date('F Y', $feedback_date);

						?>
							<div class="carousel-item is-active">
								<div class="testimonial-card">
									<div class="testimonial-card__text">
										<p><?php echo $feedback['review']; ?></p>
									</div>
									
									<div class="level is-mobile testimonial-card__meta">
										<div class="level-left">
											<div class="testimonial-card__meta-source">
												<?php echo $feedback['initials']; ?>, <?php echo $feedback_date_formatted; ?>
											</div>
										</div>
										
										<div class="level-right">
											<div class="testimonial-card__meta-rating">
												<?php get_feedback_stars($feedback['totalscore'], $feedback['unqid']); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="carousel-navigation is-centered">
						<div class="carousel-nav-left">
							<i class="fa fa-chevron-left icon--blue" aria-hidden="true"></i>
						</div>
						<div class="carousel-nav-right">
							<i class="fa fa-chevron-right icon--blue" aria-hidden="true"></i>
						</div>
					</div>
				</div>

			</div>
		</div> <!-- .columns -->

	</div>

</div> <!-- .product-side__reviews -->

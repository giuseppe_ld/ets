<?php 
	$args = array(
		'post_type' => 'affiliate',
		'posts_per_page' => '-1',
		'orderby' => 'date',
		'order' => 'ASC',
	);

	$loop = new WP_Query( $args );
	if ($loop->have_posts() > 0) :
?>

<section id="product-courses" class="content-space--top-small">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-full-tablet is-paddingless">

					<div class="columns is-paddingless is-multiline justify-sb">
						<?php
							while ( $loop->have_posts() ) : $loop->the_post(); 
							?>

							<div class="column is-full-tablet is-paddingless">
								<div class="affiliate">
									<h2 class="affiliate__title title title--blue title--small"><?php the_title(); ?></h2>
									
									<div class="level">
										<div class="level-left">
											<div class="level-item">
												<div class="affiliate__contact">
													<?php if(get_field('contact')) : ?>
														<p><?php the_field('contact'); ?></p>
													<?php endif; ?> 

													<?php if(get_field('number')) : ?>
														<p><?php the_field('number'); ?></p>
													<?php endif; ?> 

													<?php if(get_field('number')) : ?>
														<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
													<?php endif; ?> 

													<?php if(get_field('number')) : ?>
														<p><a href="<?php the_field('website'); ?>" target="_blank"><?php the_field('website'); ?></a></p>
													<?php endif; ?> 

												</div>
											</div>
										</div>
										<div class="level-right">
											<div class="level-item">
												<?php if (get_field('logo')) : ?>
													<div class="affiliate__logo">
														<?php $logo = get_field('logo'); ?>
														<img src="<?php echo $logo['url']; ?>" />
													</div>
												<?php endif; ?>
												</div>
										</div>
									</div>

									<?php if (get_field('profile')) : ?>
										<div class="affiliate__profile">
											<strong>Profile:</strong>
											<?php the_field('profile') ?>
										</div>
									<?php endif; ?>

									<?php if (get_field('comments')) : ?>
										<div class="affiliate__comments">
											<strong>Comments:</strong>
											<?php the_field('comments'); ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
						
						<?php
							endwhile;
							wp_reset_query();
						?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endif; ?>
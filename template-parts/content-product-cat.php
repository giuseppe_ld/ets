<?php

// $field = get_field_object('field_5c3dddee828df');
// $accreditors = $field['choices'];
// array_splice($accreditors, 0, 1);
?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<section id="product-filter" class="content-space--top-small">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10-desktop is-full-tablet">
				<p class="tilte title--small title--normal-case title--inline">Filter by:</p>
				<select class="product-filter product-filter__accreditation">
					<option></option>
					<option value="all">View All Accreditations</option>
					<option value="vtctapproved">VTCT</option>
					<option value="fht">Federation of Holistic Therapists</option>
					<option value="aor">Association of Reflexologists</option>
					<option value="ifpa">International Federation of Professional Aromatherapists</option>
					<option value="bowtech">Bowen</option>
					<option value="est">Essential Training Solutions</option>
					
				</select>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('template-parts/content', 'product-cat-courses'); ?>

<section id="product-revision-tools" class="content-space--top">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10-desktop is-full-tablet">
				<?php get_template_part('template-parts/content', 'product-cat-revisiontools'); ?>
			</div>
		</div>
	</div>
</section>

<section id="product-manuals" class="content-space--bottom content-space--top">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10-desktop is-full-tablet">
				<?php get_template_part('template-parts/content', 'product-cat-manuals'); ?>
			</div>
		</div>
	</div>
</section>
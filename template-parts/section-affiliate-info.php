<?php the_field('aff_info_intro'); ?>

<div class="aff-opt">
<?php 
	$aff_opts = get_field('aff_opt');
	console($aff_opts);

	foreach($aff_opts as $aff_opt) :
	
	$data_val = str_to_lowercase($aff_opt['aff_opt_title']);
	$data_val = str_spaces_to_dashes($data_val);
	?>

		<div class="aff-opt__opt">
			<div class="aff-opt__title aff-opt--dropdown" data-title="<?php echo $data_val; ?>">
				<h3 class="title title--xsmall title--white">
					<?php echo $aff_opt['aff_opt_title']; ?>
				</h3>
			</div>

			<div class="aff-opt__content" data-content="<?php echo $data_val; ?>">
				<div class="aff-opt__intro">
					<?php echo $aff_opt['aff_opt_content']; ?>
				</div>

				<?php if ( $aff_opt['aff_opt_questions'] ) : ?>
					<div class="aff-opt__qa accordions">
						<?php 
						foreach ($aff_opt['aff_opt_questions'] as $faq) : ?>
							<article class="accordion">
								<div class="accordion-header toggle">
									<?php echo $faq['aff_opt_q']; ?>
								</div>
								<div class="accordion-body">
									<div class="accordion-content">
										<?php echo $faq['aff_opt_a']; ?>
									</div>
								</div>
							</article>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php if ( $aff_opt['aff_opt_testimonials'] ) : ?>
					<div class="aff-opt__reviews">
					<div class='carousel carousel-animated carousel-animate-slide' data-size="1">
						<div class='carousel-container'>
							<?php foreach ($aff_opt['aff_opt_testimonials'] as $feedback) : ?> 
								<div class="carousel-item is-active">
									<div class="testimonial-card">
										<div class="testimonial-card__text has-text-centered">
											<?php echo $feedback['aff_opt_testimonial_content']; ?>
										</div>
										
										<div class="testimonial-card__meta-source has-text-centered">
											<?php echo $feedback['aff_opt_testimonial_source']; ?>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="carousel-navigation is-centered">
							<div class="carousel-nav-left">
								<i class="fa fa-chevron-left icon--blue" aria-hidden="true"></i>
							</div>
							<div class="carousel-nav-right">
								<i class="fa fa-chevron-right icon--blue" aria-hidden="true"></i>
							</div>
						</div>
					</div>
					</div>
				<?php endif; ?>
			</div>
		</div> <!-- .aff-opt__opt -->

		
		
		<?php 
	endforeach;
	?>
	
	<?php the_field('aff_info_end'); ?>	

</div> <!-- .aff-opt -->
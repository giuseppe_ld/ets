<?php
$query = new WC_Product_Query( array(
	'limit' => 3,
	'orderby' => 'date',
	'order' => 'ASC',
	'stock_status' => 'instock',
	'featured' => true,
	
	 ) );
	
	 $products = $query->get_products();
?>
<div class="section">
		<div class="columns popular-courses is-centered">
			<div class="column is-9-widescreen is-10-desktop is-12-tablet">
				<div class="section">
					<div class="columns">
						<?php foreach ($products as $product) : ?>
						<?php
							$course_info = get_field('course_group', $product->get_id());
						?>
							<div class="column is-11-smobile is-10-mobile is-centered-mobile">
								<div class="card">
									<div class="card-image">
										<figure class="image popular-courses__image">
											<?php if ($course_info['accreditation']) { ?>
												<a href="<?php echo $product->get_permalink(); ?>" title="<?php echo $product->get_title();?>">
													<img src="<?php echo ets_display_accreditor($course_info['accreditation']); ?>" alt="<?php echo $product->get_title(); ?>" class="product-box__accreditation">
												</a>
											<?php 
											} else if (get_the_post_thumbnail_url($product->get_id())) : ?>
												<a href="<?php echo $product->get_permalink(); ?>" title="<?php echo $product->get_title()	;?>">
													<img src="<?php echo get_the_post_thumbnail_url($product->get_id()); ?>" alt="<?php echo $product->get_title(); ?>">
												</a>
											<?php else : ?>
												<img src="https://bulma.io/images/placeholders/128x128.png">
											<?php endif; ?>
										</figure>
									</div>
									<div class="card-content">

										<div class="content">
											<p class="title--semibold title--xsmall title--blue"><?php echo $product->get_title(); ?></p>

											<div class="level">
												<div class="level-left">
													<div class="level-item title--small title--blue">
														<?php get_product_price($product->get_id()); ?>
													</div>
												</div>
												
												<div class="level-right">
													<div class="level-item">
														<div>
															<a href="<?php echo $product->get_permalink(); ?>" class="btn btn--blue btn--hover-yellow">View course</a>
														</div>
													</div>
	
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div><!-- .section -->
			</div>
		</div> <!-- .columns.popular-courses -->
</div> <!-- .section -->
<?php
$info = get_fields('options');
?>

<section id="contact" class="content-space--bottom content-space--top">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10">
				<div class="columns">
					<div class="column is-5">
						
						<h3 class="title title--small title--blue has-text-centered-mobile">Contact details</h3>

						<?php echo $info['address']; ?>

						<p><i class="fas fa-phone icon--blue icon--rgap"></i><?php echo $info['gen_phone_number']; ?></p>

						<p><i class="fas fa-envelope icon--blue icon--rgap"></i><a href="mailto:<?php echo $info['gen_email']; ?>" title="Email Essential Training Solutions"><?php echo $info['gen_email']; ?></a></p>

						<p><i class="fas fa-calendar-alt icon--blue icon--rgap"></i><?php echo $info['opening_days']; ?></p>

						<p><i class="fas fa-clock icon--blue icon--rgap"></i><?php echo $info['opening_hours']; ?></p>
					</div>
					
					<div class="column is-2" style="position:relative;">
						<div class="vl"></div>
					</div>

					<div class="column is-5">
						<h3 class="title title--small title--blue has-text-centered-mobile">Send us a message</h3>
						
						<?php echo do_shortcode('[contact-form-7 id="138" title="Contact form 1"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
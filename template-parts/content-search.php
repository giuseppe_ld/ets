<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Essential_Training_Solutions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('content-space--top content-space--bottom'); ?>>

	<div class="columns is-centered">
		<div class="column is-9">
			<div class="columns">
			<?php if (has_post_thumbnail()) : ?>
			<div class="column is-4">
				<?php essential_training_post_thumbnail(); ?>
			</div>

			<div class="column is-8">
				<header class="entry-header">
					<?php the_title( sprintf( '<h3 class="entry-title title title--small"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
		
					<?php if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta">
						<?php
						essential_training_posted_on();
						essential_training_posted_by();
						?>
					</div><!-- .entry-meta -->
					<?php endif; ?>
				</header><!-- .entry-header -->
		
		
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
			</div>
			<?php endif; ?>
			</div>
		</div>
		
	</div>

</article><!-- #post-<?php the_ID(); ?> -->

<?php

$related_product_id = get_field('related_product');

$query = new WC_Product_Query( array(
	'limit' => 1,
	'stock_status' => 'instock',
	'include' => array($related_product_id), 
	 ) );
	
	 $products = $query->get_products();
?>
<div class="section">
		<div class="columns related-courses is-centered">
			<div class="column is-full">
				<?php foreach ($products as $product) : ?>
				<?php $course_info = get_field('course_group', $product->get_id());
				?>
					<div class="card">
						<div class="card-image">
							<figure class="image related-courses__image">
								<?php if ($course_info['accreditation']) : ?>
									<a href="<?php echo $product->get_permalink(); ?>" title="<?php echo $product->get_title();?>">
										<img src="<?php echo ets_display_accreditor($course_info['accreditation']); ?>" alt="<?php echo $product->get_title(); ?>" class="product-box__accreditation">
									</a>
								<?php else : ?>
									<img src="https://bulma.io/images/placeholders/128x128.png">
								<?php endif; ?>
							</figure>
						</div>
						<div class="card-content">

							<div class="content">
								<p class="title--semibold title--small title--blue">
									<a href="<?php echo $product->get_permalink(); ?>" title="<?php echo $product->get_title()	;?>">
										<?php echo $product->get_title(); ?>
									</a>
								</p>

								<div class="level">
									<div class="level-left">
										<div class="level-item title--small title--blue">
											<?php get_product_price($product->get_id()); ?>
										</div>
									</div>
									
									<div class="level-right">
										<div class="level-item">
											<div>
												<a href="<?php echo $product->get_permalink(); ?>" class="btn btn--blue btn--hover-yellow">View course</a>
											</div>
										</div>

									</div>
								</div>
								
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div> <!-- .columns.related-courses -->
</div> <!-- .section -->
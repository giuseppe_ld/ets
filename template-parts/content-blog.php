<article id="post-<?php the_ID(); ?>" <?php post_class('column is-6-tablet is-4-desktop'); ?>>	
	<a href="<?php the_permalink(); ?>">
		<div class="blog-post__image" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');" ?></div>
		
		<div class="blog-post__content">
			<header class="entry-header">
				<?php
					the_title( '<h1 class="entry-title">', '</h1>' );

					?>
					<div class="entry-meta">
						<?php
						essential_training_posted_on();
						?>
					</div><!-- .entry-meta -->
			</header><!-- .entry-header -->
		</div>
	</a>
</article><!-- #post-<?php the_ID(); ?> -->


<?php
$faqs = get_field('faq');
?>

<section id="faqs" class="content-space--bottom content-space--top">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-9">
				<section class="accordions">
					<?php 
					$i = 0;
					foreach ($faqs as $faq) : ?>
						<article class="accordion">
							<div class="accordion-header toggle">
								<?php echo $faq['question']; ?>
							</div>
							<div class="accordion-body">
								<div class="accordion-content">
									<?php echo $faq['answer']; ?>
								</div>
							</div>
						</article>
					<?php endforeach; ?>
				</section>
			</div>
		</div>
	</div>
</section>
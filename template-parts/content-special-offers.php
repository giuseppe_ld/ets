<?php 
$args = array(
	'post_type' => 'product',
	'posts_per_page' => '-1',
	'orderby' => 'id',
	'order' => 'ASC',
	'tax_query' =>	array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => 'special-offers',
		)
	)
);

$loop = new WP_Query( $args );
if ($loop->have_posts() > 0) : ?>
<section id="product-courses" class="content-space--top-small">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10-desktop is-full-tablet">
<?php
endif;

while ( $loop->have_posts() ) : $loop->the_post(); 

$is_course = get_field('product_type') === 'course' ? true : false;

	if($is_course) :
	$course_accred = '';

	if (get_field('product_type') === 'course') {
		$course_info = get_field('course_group');
		if ($course_info['accreditation'] == 'fhtaccred' || $course_info['accreditation'] == 'fhtshort') {
			$course_accred = 'fht';
		} else {
			$course_accred = $course_info['accreditation'];
		}
		
	}
endif;
?>


<?php if ($is_course) : ?>
	<article class="media product-box product-box--course product-<?php echo $post->ID; ?>" data-accreditation="<?php echo $course_accred; ?>">
		<figure class="media-left is-marginless is-4 product-box__image">
			<p class="image">
				<?php if ($course_info['accreditation']) : ?>
					<a href="/basket/?add-to-cart=<?php echo $post->ID; ?>">
						<img class="product-box__accreditation" src="<?php echo ets_display_accreditor($course_info['accreditation']); ?>"
							alt="<?php echo $course_info['qualification_title']; ?>" />
					</a>
				<?php else : ?>
					<img src="https://bulma.io/images/placeholders/128x128.png">
				<?php endif; ?>
			</p>
		</figure>
		<div class="media-content product-box__content">
			<div class="content product-box__info">

				<div class="columns is-mobile">
					<div class="column is-paddingless">
						<div>
							<h3 class="product-box__title"><a href="/basket/?add-to-cart=<?php echo $post->ID; ?>&discount=college">
									<?php echo $post->post_title; ?></a></h3>
							<?php 
							if (get_field('product_type') === 'course') : ?>
								<h5 class="product-box__subtitle">
									<?php echo $course_info['qualification_title']; ?>
								</h5>
							<?php endif; ?>
						</div>
					</div>
				</div>

				
				
				<div class="product-box__desc">
					<?php echo get_the_excerpt(); ?>
				</div>

				<div class="level is-mobile">
					<div class="level-left">
						<div class="level-item">
							<span class="product-box__price"><?php get_product_price($post->ID); ?></span>
						</div>
					</div>
					<div class="level-right">
						<div class="level-item">
							<a href="/basket/?add-to-cart=<?php echo $post->ID; ?>&discount=college" class="btn btn--outline-blue btn--hover-yellow">Order Now</a>
						</div>
					</div>
				</div>
			</div>

			<div class="level is-mobile product-box__meta">
				<div class="level-left">
					<div class="level-item">
						<div class="product-box__meta-rating">
							<?php get_overall_product_feedback($post->ID); ?>
						</div>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<?php if (get_field('product_type') === 'course') : ?>
							<span class="product-box__meta-qualification">
								<?php echo ets_get_qualification($course_info); ?>
							</span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php else : ?>
<article class="media product-box product-box--manuals product-<?php echo $post->ID; ?>">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<?php if (get_the_post_thumbnail_url()) : ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
				</a>
			<?php else : ?>
				<img src="https://bulma.io/images/placeholders/128x128.png">
			<?php endif; ?>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">
			<h3 class="product-box__title"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
			<p class="product-box__desc">
				<?php echo get_the_excerpt(); ?>
			</p>

			<div class="level">
				<div class="level-left">
					<div class="level-item">
					<span class="offers__regular-price"><?php get_product_price($post->ID); ?></span>&nbsp;&nbsp;
						<span class="product-box__price"> £<?php echo get_special_offer_price($post->ID, 'college'); ?></span>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/basket/?add-to-cart=<?php echo $post->ID; ?>&discount=college" class="btn btn--outline-blue btn--hover-yellow">Order Now</a>
					</div>
					<div class="level-item">
						<a href="<?php the_permalink(); ?>" class="btn btn--blue btn--hover-yellow">More info</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php endif; ?>

<?php 
endwhile;
wp_reset_query();
?>

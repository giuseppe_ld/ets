<article class="media product-box product-box--course freebies">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<a href="/freebies/free-tutorial-download/?course-demo=Protection-From-Abuse" title="7. Protection from Abuse">
				<img class="product-box__accreditation" src="/wp-content/uploads/freebies-protection.jpg" alt="">
			</a>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">

			<div class="columns is-mobile">
				<div class="column is-paddingless">
					<div>
						<h3 class="product-box__title"><a href="/freebies/free-tutorial-download/?course-demo=Protection-From-Abuse">
						Maintaining Best Practice - Protection from Abuse</a></h3>
					</div>
				</div>
			</div>



			<div class="product-box__desc">
				<p>Please enjoy our short Protection From Abuse online tutorial. This is an excellent opportunity to see if you would enjoy e-learning at no cost, whilst studying a subject that is relevant to all therapists. Both the FHT and AoR award CPD points upon successful completion.</p>
			</div>

			<div class="level is-mobile">
				<div class="level-left">
					<div class="level-item">
						<span class="product-box__price">FREE</span>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/freebies/free-tutorial-download/?course-demo=Protection-From-Abuse" class="btn btn--blue btn--hover-yellow">View Now</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</article>

<article class="media product-box product-box--course freebies">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<a href="/freebies/free-tutorial-download/?course-demo=Anatomy-Physiology" title="Anatomy & Physiology Free Course Demo">
				<img class="product-box__accreditation" src="/wp-content/uploads/freebies-anatomy.jpg" alt="">
			</a>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">

			<div class="columns is-mobile">
				<div class="column is-paddingless">
					<div>
						<h3 class="product-box__title"><a href="/freebies/free-tutorial-download/?course-demo=Anatomy-Physiology">Anatomy & Physiology - Free Course Demo</a></h3>
					</div>
				</div>
			</div>



			<div class="product-box__desc">
				<p>This free course demonstration provides you with a few screens from every section of the Essential Anatomy and Physiology tutorial.</p>
			</div>

			<div class="level is-mobile">
				<div class="level-left">
					<div class="level-item">
						<span class="product-box__price">FREE</span>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/freebies/free-tutorial-download/?course-demo=Anatomy-Physiology" class="btn btn--blue btn--hover-yellow">View Now</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</article>

<article class="media product-box product-box--course freebies">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<a href="/freebies/free-tutorial-download/?course-demo=Fire-Evacuation" title="Fire & Evacuation - Free Course Demo">
				<img class="product-box__accreditation" src="/wp-content/uploads/freebies-fire.jpg" alt="">
			</a>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">

			<div class="columns is-mobile">
				<div class="column is-paddingless">
					<div>
						<h3 class="product-box__title"><a href="/freebies/free-tutorial-download/?course-demo=Fire-Evacuation">Fire & Evacuation - Free Course Demo</a></h3>
					</div>
				</div>
			</div>



			<div class="product-box__desc">
				<p>This free course demonstration provides you with a few screens from the Fire and Evacuation tutorial. </p>
			</div>

			<div class="level is-mobile">
				<div class="level-left">
					<div class="level-item">
						<span class="product-box__price">FREE</span>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/freebies/free-tutorial-download/?course-demo=Fire-Evacuation" class="btn btn--blue btn--hover-yellow">View Now</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</article>

<article class="media product-box product-box--course freebies">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<a href="/freebies/free-tutorial-download/?course-demo=Health-Safety" title="Health & Safety - Free Course Demo">
				<img class="product-box__accreditation" src="/wp-content/uploads/freebies-health-safety.jpg" alt="">
			</a>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">

			<div class="columns is-mobile">
				<div class="column is-paddingless">
					<div>
						<h3 class="product-box__title"><a href="/freebies/free-tutorial-download/?course-demo=Health-Safety">Health & Safety - Free Course Demo</a></h3>
					</div>
				</div>
			</div>



			<div class="product-box__desc">
				<p>This free course demonstration shows a few screens from the Health & Safety tutorial.</p>
			</div>

			<div class="level is-mobile">
				<div class="level-left">
					<div class="level-item">
						<span class="product-box__price">FREE</span>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/freebies/free-tutorial-download/?course-demo=Health-Safety" class="btn btn--blue btn--hover-yellow">View Now</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</article>



<h3 class="title title--small title--blue">Stay up to date with our latest news and offers</h3>
<form id="mxm-form-272" action="https://mxm.mxmfb.com/form/process/c/2865/f/5cade5908c571" class="mxm-form mxm-form-populate form__group--signup go" method="post">
		<div class="form__group">
			<div id="mxm-form-272-field-0"><label class="mxm-form-item-label form-field__label" for="mxm-form-272-field-0-el">Email Address<em>*</em></label>
					<div class="mxm-form-field mxm-required"><input type="text" name="email_address" size="40" maxsize="100" id="mxm-form-272-field-0-el" class="mxm-form-element form-field__input mxm-form-text mxm-validation-email" /></div>
			</div>
		</div>
		<div class="form__group">
			<div id="mxm-form-272-field-1"><label class="mxm-form-item-label form-field__label" for="mxm-form-272-field-1-el">First Name</label>
					<div class="mxm-form-field"><input type="text" name="Web subscribers.First Name" value="" size="40" maxsize="100" id="mxm-form-272-field-1-el" class="mxm-form-element form-field__input mxm-form-text mxm-validation-none" /></div>
			</div>
		</div>
		<div class="form__group">
			<div id="mxm-form-272-field-2"><label class="mxm-form-item-label form-field__label" for="mxm-form-272-field-2-el">How did you hear about us?</label>
					<div class="mxm-form-field"><input type="text" name="Web subscribers.How did hear about us" value="" size="40" maxsize="100" id="mxm-form-272-field-2-el" class="mxm-form-element form-field__input mxm-form-text mxm-validation-none" /></div>
			</div>
		</div>
		<div id="mxm-form-272-field-3">
				<div class="mxm-form-field"><input type="hidden" name="Web subscribers.Subscribed2" value="no" /><input id="mxm-form-272-field-3-el" type="checkbox" name="Web subscribers.Subscribed2" value="yes" class="mxm-form-element mxm-form-booleancheckbox" /><label for="mxm-form-272-field-3-el" class="mxm-form-cb-label">I agree to receive the ETS newsletters and bulletins.</label></div>
		</div>
		<div class="form__group" style="margin-top: 20px;">
			<div class="mxm-form-item" id="mxm-form-272-field-4">
					<div class="mxm-form-field"><label class="mxm-form-item-label"></label><input type="submit" value="Submit" class="mxm-form-element mxm-form-button btn btn--blue btn--hover-yellow" /></div>
			</div>
		</div>
</form>

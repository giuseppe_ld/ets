<?php 
$args = array(
	'post_type' => 'product',
	'posts_per_page' => '-1',
	'orderby' => 'id',
	'order' => 'ASC',
	'tax_query' =>	array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => $post->post_name,
		),
		array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => 'courses',
		)
	)
);

$is_mbp = $post->post_name === 'maintaining-best-practice' ? true : false;


$loop = new WP_Query( $args );
if ($loop->have_posts() > 0) : ?>
<section id="product-courses" class="content-space--top-small">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10-desktop is-full-tablet">
<h2 class="product-section__title">Courses</h2>

<?php
endif;

while ( $loop->have_posts() ) : $loop->the_post(); 

$course_accred = '';

if (get_field('product_type') === 'course') {
	$course_info = get_field('course_group');
	if ($course_info['accreditation'] == 'fhtaccred' || $course_info['accreditation'] == 'fhtshort') {
		$course_accred = 'fht';
	} else {
		$course_accred = $course_info['accreditation'];
	}
	
}

?>


<?php if ($is_mbp) : ?>
	<article class="media product-box product-box--course product-<?php echo $post->ID; ?>" data-accreditation="<?php echo $course_accred; ?>">
		<figure class="media-left is-marginless is-4 product-box__image">
			<p class="image">
				<?php if ($course_info['accreditation']) : ?>
					<a href="/basket/?add-to-cart=<?php echo $post->ID; ?>">
						<img class="product-box__accreditation" src="<?php echo ets_display_accreditor($course_info['accreditation']); ?>"
							alt="<?php echo $course_info['qualification_title']; ?>" />
					</a>
				<?php else : ?>
					<img src="https://bulma.io/images/placeholders/128x128.png">
				<?php endif; ?>
			</p>
		</figure>
		<div class="media-content product-box__content">
			<div class="content product-box__info">

				<div class="columns is-mobile">
					<div class="column is-paddingless">
						<div>
							<h3 class="product-box__title"><a href="/basket/?add-to-cart=<?php echo $post->ID; ?>">
									<?php echo $post->post_title; ?></a></h3>
							<?php 
							if (get_field('product_type') === 'course') : ?>
								<h5 class="product-box__subtitle">
									<?php echo $course_info['qualification_title']; ?>
								</h5>
							<?php endif; ?>
						</div>
					</div>
				</div>

				
				
				<div class="product-box__desc">
					<?php echo get_the_excerpt(); ?>
				</div>

				<div class="level is-mobile">
					<div class="level-left">
						<div class="level-item">
							<span class="product-box__price"><?php get_product_price($post->ID); ?></span>
						</div>
					</div>
					<div class="level-right">
						<div class="level-item">
							<a href="/basket/?add-to-cart=<?php echo $post->ID; ?>" class="btn btn--outline-blue btn--hover-yellow">Order Now</a>
						</div>
					</div>
				</div>
			</div>

			<div class="level is-mobile product-box__meta">
				<div class="level-left">
					<div class="level-item">
						<div class="product-box__meta-rating">
							<?php get_overall_product_feedback($post->ID); ?>
						</div>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<?php if (get_field('product_type') === 'course' && get_field('cer_dip') === 'none') : ?>
							<span class="product-box__meta-qualification">
								<?php echo ets_get_qualification($course_info); ?>
							</span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php else : ?>
	<article class="media product-box product-box--course product-<?php echo $post->ID; ?>" data-accreditation="<?php echo $course_accred; ?>">
		<figure class="media-left is-marginless is-4 product-box__image">
			<p class="image">
				<?php if ($course_info['accreditation']) : ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
						<img class="product-box__accreditation" src="<?php echo ets_display_accreditor($course_info['accreditation']); ?>"
							alt="<?php echo $course_info['qualification_title']; ?>" />
					</a>
				<?php else : ?>
					<img src="https://bulma.io/images/placeholders/128x128.png">
				<?php endif; ?>
			</p>
		</figure>
		<div class="media-content product-box__content">
			<div class="content product-box__info">

				<div class="columns is-mobile">
					<div class="column is-paddingless">
						<div>
							<h3 class="product-box__title"><a href="<?php the_permalink(); ?>">
									<?php echo $post->post_title; ?></a></h3>
							<?php 
							if (get_field('product_type') === 'course') : ?>
								<h5 class="product-box__subtitle">
									<?php echo $course_info['qualification_title']; ?>
								</h5>
							<?php endif; ?>
						</div>
					</div>
				</div>

				
				
				<div class="product-box__desc">
					<?php echo get_the_excerpt(); ?>
				</div>

				<div class="level is-mobile">
					<div class="level-left">
						<div class="level-item">
							<span class="product-box__price"><?php get_product_price($post->ID); ?></span>
						</div>
					</div>
					<div class="level-right">
						<div class="level-item">
							<a href="/basket/?add-to-cart=<?php echo $post->ID; ?>" class="btn btn--outline-blue btn--hover-yellow">Order Now</a>
						</div>

						<div class="level-item">
							<a href="<?php the_permalink(); ?>" class="btn btn--blue btn--hover-yellow">More info</a>
						</div>
					</div>
				</div>
			</div>

			<div class="level is-mobile product-box__meta">
				<div class="level-left">
					<div class="level-item">
						<div class="product-box__meta-rating">
						<?php get_overall_product_feedback($post->ID); ?>
						</div>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<?php if (get_field('product_type') === 'course') : ?>
							<span class="product-box__meta-qualification">
								<?php echo ets_get_qualification($course_info); ?>
							</span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php endif; ?>

<?php 
endwhile;
wp_reset_query();
?>

<?php if ($is_mbp) : ?>

<article class="media product-box product-box--course product-280" data-accreditation="etslogo">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<a href="/freebies/free-tutorial-download/?course-demo=Protection-From-Abuse" title="7. Protection from Abuse">
				<img class="product-box__accreditation" src="/wp-content/uploads/accreditors/etslogo.jpg" alt="">
			</a>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">

			<div class="columns is-mobile">
				<div class="column is-paddingless">
					<div>
						<h3 class="product-box__title"><a href="/freebies/free-tutorial-download/?course-demo=Protection-From-Abuse">
								7. Protection from Abuse</a></h3>
						<h5 class="product-box__subtitle">
						</h5>
					</div>
				</div>
			</div>



			<div class="product-box__desc">
				<ul>
					<li>Create a safe environment for the consultation and treatment to take place</li>
					<li>Understand the basic rules when treating minors and other vulnerable groups</li>
					<li>Protect yourself from abuse</li>
				</ul>
			</div>

			<div class="level is-mobile">
				<div class="level-left">
					<div class="level-item">
						<span class="product-box__price">FREE</span>
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/freebies/free-tutorial-download/?course-demo=Protection-From-Abuse" class="btn btn--outline-blue btn--hover-yellow">View Now</a>
					</div>

				</div>
			</div>
		</div>

		<div class="level is-mobile product-box__meta">
			<div class="level-left">
				<div class="level-item">
					<div class="product-box__meta-rating">
						
					</div>
				</div>
			</div>
			<div class="level-right">
				<div class="level-item">
					<span class="product-box__meta-qualification">
					</span>
				</div>
			</div>
		</div>
	</div>
</article>

<p>Please note the 7 maintaining best practice tutorials are not assessment-based courses and neither completing the tutorials nor printing the certificates in any way qualifies you in these subjects. However, all seven units are included in the <a href="/product/essential-health-and-safety-online-diploma-course-fht/" title="Essential Health and Safety Online Diploma Course"><strong>FHT-Approved Level 3 Diploma course</strong></a>. This course is assessment-based and a formal certificate is awarded on successful completion.</p>

<?php
endif;

if ($post->post_name === 'health-and-safety') : ?>
<article class="media product-box product-box--course product-mbp" data-accreditation="<?php echo $course_info['accreditation']; ?>">
	<figure class="media-left is-marginless is-4 product-box__image">
		<p class="image">
			<a href="/courses/health-and-safety/maintaining-best-practice/" title="Maintaining Best Practice Course">
				<img class="product-box__accreditation" src="/wp-content/uploads/accreditors/etslogo.jpg" />
			</a>
		</p>
	</figure>
	<div class="media-content product-box__content">
		<div class="content product-box__info">

			<div class="columns is-mobile">
				<div class="column is-paddingless">
					<div>
						<h3 class="product-box__title">
							<a href="/courses/health-and-safety/maintaining-best-practice/">
								Maintaining Best Practice
							</a>
						</h3>
					</div>
				</div>
			</div>

			
			
			<p class="product-box__desc">
			Here at ETS we know how difficult it can be for therapists to keep up to date with information. To this end we have 7 Maintaining Best Practice tutorials to help keep your knowledge of current working practices up to date plus a great software package to help you to run your business. For each tutorial you can automatically print a Certificate of Participation for your CPD portfolio when you complete it. When you purchase a tutorial we send you your login details and you take the tutorial from within our structured, secure learning management system. Access is licenced for one year.
			</p>

			<div class="level is-mobile">
				<div class="level-left">
					<div class="level-item">
						&nbsp;
					</div>
				</div>
				<div class="level-right">
					<div class="level-item">
						<a href="/courses/health-and-safety/maintaining-best-practice/" class="btn btn--blue btn--hover-yellow">View courses</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php endif; ?>

			</div>
		</div>
	</div>
</section>
<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

?>
<div class="section">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-10">

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
				<div class="post__image" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');" ?></div>
				
				<div class="post__content">
					<header class="entry-header">
						<?php
							the_title( '<h1 class="entry-title">', '</h1>' );

							?>
							<div class="entry-meta">
								<?php
								essential_training_posted_on();
								?>
							</div><!-- .entry-meta -->
					</header><!-- .entry-header -->
					<div class="entry-content">
						<?php
						the_content( sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'essential-training' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						) );

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'essential-training' ),
							'after'  => '</div>',
						) );
						?>
					</div><!-- .entry-content -->
				</div>
				</article><!-- #post-<?php the_ID(); ?> -->


			</div>
		</div>
	</div>
</div>

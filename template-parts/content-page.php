<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Essential_Training_Solutions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'essential-training' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
		if (is_page('affiliates')) :
			get_template_part( 'template-parts/section', 'affiliate-list' );
		endif;
	?>

	<?php
		if (is_page('ets-affiliate-information')) :
			get_template_part( 'template-parts/section', 'affiliate-info' );
		endif;
	?>

	<?php
		if (is_page('freebies')) :
			get_template_part( 'template-parts/section', 'freebies' );
		endif;
	?>

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer" style="background-color: transparent; color: black;">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'essential-training' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->

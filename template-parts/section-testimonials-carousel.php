<?php 

$testimonials = get_featured_product_feedback(); ?>


<div class="columns is-centered">
	<div class="column is-9">

		<div class="homepage-testimonials owl-carousel owl-theme">
			<?php

			foreach($testimonials as $testimonial) : 
				$testimonial_date = date('F Y', strtotime($testimonial['date']));
				?>
			
			<div>
				<div class="testimonial-card">
					<div class="testimonial-card__text">
						<p><strong><?php echo $testimonial['CourseNice'] ?></strong></p>
						<p><?php echo $testimonial['review'] ?></p>
					</div>
					
					<div class="level is-mobile testimonial-card__meta">
						<div class="level-left">
							<div class="testimonial-card__meta-source">
								<?php echo $testimonial['initials'] ?>, <?php echo $testimonial_date; ?>
							</div>
						</div>
						
						<div class="level-right">
							<div class="testimonial-card__meta-rating">
								<?php get_feedback_stars($testimonial['totalscore'], $testimonial['unqid']); ?>
							</div>
						</div>
					</div>
				</div>
			</div> 
			<?php endforeach; ?>

		</div>

	</div>
</div>


<?php
$hiw = get_field('how_it_works');
?>

<section id="how-it-works" class="content-space--bottom content-space--top">
	<div class="container">
		<div class="columns is-centered">
			<div class="column is-11-tablet">
				<div class="columns">
					<div class="column is-hidden-mobile how-group">

						<?php 
						$point_count = 1; 

						
						foreach ($hiw as $hiw_group) :
							foreach ($hiw_group as $hiw_point) :
								if ($point_count == 1) : ?>
									<div class="columns is-vcentered how-group__points is-active" data-point="<?php echo $point_count; ?>">
										<div class="column is-1 is-paddingless">
											<div class="how-group__number has-text-centered">
												<?php echo $point_count; ?>
											</div>
										</div>
										<div class="column how-group__heading">
											<h3><?php echo $hiw_point['hiw_point']; ?></h3>
										</div>
									</div>
								<?php else : ?>
									<div class="columns is-vcentered how-group__points" data-point="<?php echo $point_count; ?>">
										<div class="column is-1 is-paddingless">
											<div class="how-group__number has-text-centered">
												<?php echo $point_count; ?>
											</div>
										</div>
										<div class="column how-group__heading">
											<h3><?php echo $hiw_point['hiw_point']; ?></h3>
										</div>
									</div>

								<?php endif;
							endforeach; 
							$point_count++;
						endforeach; 
						
						?>

					</div>

					<div class="column">

						<?php
						$info_count = 1;

						foreach ($hiw as $hiw_group) :
							foreach ($hiw_group as $hiw_info) :

								if ($info_count == 1) : ?>
									<div class="how-group__info is-active" data-info="<?php echo $info_count; ?>">
										<div class="how-group__info-number"><?php echo $info_count; ?></div>
										<?php echo $hiw_info['hiw_info']; ?>
									</div>
									
								<?php else :?>
									
									<div class="how-group__info" data-info="<?php echo $info_count; ?>">
									<div class="how-group__info-number"><?php echo $info_count; ?></div>
										<?php echo $hiw_info['hiw_info']; ?>
									</div>
								<?php endif; 
							endforeach; 
							$info_count++;
							?>

						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
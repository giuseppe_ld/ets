<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Twice_Agency
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="section">
				<div class="container">
					<div class="columns is-centered">
						<div class="column is-10">
							<div class="columns is-centered is-multiline">

						
									<?php
									while ( have_posts() ) :
										the_post();
										
											get_template_part( 'template-parts/content', 'blog' );

									endwhile; // End of the loop.
									?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();


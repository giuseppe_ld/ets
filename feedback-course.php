<?php 
/* Template Name: Feedback - Course  */ 

get_header();
global $wpdb;
$sucess=0;
$country="";$courseID="";$easeofuse="";$content="";$valueformoney="";$tutorsupport="";$firstname="";$lastname="";$fname="";$lname="";$postcode="";$localtown="";$email="";$q_improve="";$review="";
if(isset($_POST['new_hidden'])) {
	$addedit=$_POST['new_hidden'];

	if($_POST['firstname']){$firstname=$_POST['firstname'][0];$fname=$_POST['firstname'];}

	if($_POST['lastname']){$lastname=$_POST['lastname'][0];$lname=$_POST['lastname'];}

	$postcode=$_POST['postcode'];
	$localtown=$_POST['town'];
	$country=$_POST['country'];
	$courseID=$_POST['course'];
	$easeofuse=$_POST['ease_of_use'];
	$content=$_POST['content'];
	$tutorsupport=$_POST['tutor_support'];
	$valueformoney=$_POST['value_for_money'];
	$review=$_POST['review'];
	$custapprove=$_POST['cust_approve'];
	$totalscore=intval($easeofuse)+intval($content)+intval($tutorsupport)+intval($valueformoney);
	$stars=(5/40)*$totalscore;
	
	if($courseID) {
		$productname = wc_get_product( $courseID );
		$courseName=$productname->get_title();
	}
	
	if($firstname == "" || $lastname == "" || $postcode == "" || $localtown == ""  || $country == "" || $courseID == "" || $easeofuse == 0 || $content == 0 || $tutorsupport == 0 || $valueformoney == 0 ) {
		$sucess=2;
	} else {
		$wpdb->insert('feedbackdata',array('courseID'=>$courseID,'CourseNice'=>$courseName,'date'=>date("Y/m/d"),'initials'=>$firstname.$lastname,'postcode'=>$postcode,'town'=>$localtown,'country'=>$country,'easeofuse'=>$easeofuse,'content'=>$content,'tutorsupport'=>$tutorsupport,'vfm'=>$valueformoney,'totalscore'=>$totalscore,'stars'=>$stars,'review'=>$review,'custapprove'=>$custapprove,'etsapprove'=>'no'),array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'));
			$sucess=1;
		$country="";$courseID="";$easeofuse="";$content="";$valueformoney="";$tutorsupport="";$firstname="";$lastname="";$fname="";$lname="";$postcode="";$localtown="";$email="";$q_improve="";$review="";
	}
}
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main section">
		<div class="container">
			<div class="columns is-centered">
				<div class="column is-11-widescreen is-10-desktop is-10-tablet">
					<?php if($sucess == 2) {?>
						<br><p align="center" style="color: red;">Fields marked with * are mandatory!</p><?php }elseif($sucess == 1){?><br>
							<p align="center" style="color: green;">Thank you for the feedback!</p>
					<?php }?>

					<div style="padding-bottom: 20px;">
						<form action="" method="post" id="feedbackform-course">
						
							<fieldset style="padding-top: 20px;padding-bottom: 20px;">
								<strong>Personal Details</strong>

								<div class="form__group" style="margin-top: 20px;">
									<label class="form-field__label">First Name <span style="color: red;">*</span></label>
									<input class="form-field__input" type="text" size="45" name="firstname" value="<?php echo $fname;?>">
								</div>

								<div class="form__group">
									<label class="form-field__label">Last Name <span style="color: red;">*</span></label>
									<input class="form-field__input" type="text" size="45" name="lastname" value="<?php echo $lname;?>">
								</div>
																										
																									
								<div class="form__group">
									<label class="form-field__label">Postcode <span style="color: red;">*</span></label>
									<input class="form-field__input" type="text" size="45" name="postcode" value="<?php echo $postcode;?>" >
								</div>

								<div class="form__group">
									<label class="form-field__label">Local Town <span style="color: red;">*</span></label>
									<input class="form-field__input" type="text" size="45" name="town" value="<?php echo $localtown;?>" >
								</div>

								<div class="form__group">
									<label class="form-field__label--select">Country <span class="catchchacolor">(if outside UK)</span></label>
									<select name="country" id="country" style="width: 350px; height: 30px;">
										<?php
										$countries = $wpdb->get_results("SELECT * FROM feedback_countries");
										if($countries) {
											if(!$country) {
												$country='UK';
											}
											
											if($country == 'UK') { ?>
												<option value="UK">United Kingdom</option><?php
											} else { ?>
												<option value="<?php echo $country;?>"><?php echo $country;?></option><?php
											}
											
											foreach($countries as $countries_data1 => $countries_row1) {
												if($countries_row1->country_name != $country) { ?>
													<option value="<?php echo $countries_row1->country_name;?>"><?php echo $countries_row1->country_name;?></option><?php
												}
											}
										} ?>
									</select>
								</div>


								<div class="form__group">
									<label class="form-field__label--select">Course Name <span style="color: red;">*</span></label>
									<select name="course" id="course" style="width: 350px; height: 30px;">
										<?php
											$args = array(
												'post_type'      => 'product',
												'posts_per_page' => -1,
												'product_cat'    => 'courses'
											);

											$loop = new WP_Query( $args );
											if(!$courseID) {
												$courseID = 0;
												$courseName ='-Select-';
											} else {
												$productname = wc_get_product( $courseID );
												$courseName=$productname->get_title();
											} ?>
											
												<option value="<?php echo $courseID;?>"><?php echo $courseName;?></option>
											<?php

											while ( $loop->have_posts() ) : $loop->the_post();
													global $product;
													if($courseID != get_the_ID()) { ?>
														<option value="<?php echo get_the_ID();?>"><?php echo get_the_title();?></option>
													
													<?php }
											endwhile;
											?>
									</select>
								</div>
							</fieldset>
													
													
														
							<fieldset>
								<strong>Please score the course out of 10 for the following by selecting the appropriate number - where 1 is poor and 10 is excellent.</strong>
								
								<div class="form__group" style="margin-top: 26px;">
									<label class="form-field__label--select">Ease of Use  <span style="color: red;">*</span></label>
									<select name="ease_of_use" id="ease_of_use" >
										<?php if(!$easeofuse) { ?>
											<option value="0">-Select-</option>
										<?php } else { ?>
											<option value="<?php echo $easeofuse;?>"><?php echo $easeofuse;?></option>
											<option value="0">-Select-</option>
										<?php }
												
										for($i=1;$i<=10;$i++) {
											if($easeofuse != $i) { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option><?php
											}
										}
										?>
									</select>
								</div>
														
														
								<div class="form__group">
									<label class="form-field__label--select">Content  <span style="color: red;">*</span></label>
									<select name="content" id="content" >
									<?php if(!$content){?>
										<option value="0">-Select-</option>
									<?php } else { ?>
										<option value="<?php echo $content;?>"><?php echo $content;?></option>
										<option value="0">-Select-</option>
									<?php }

									for($i=1;$i<=10;$i++) {
										if($content != $i) { ?>
											<option value="<?php echo $i;?>"><?php echo $i;?></option>
										<?php }
									}
									?>
									</select>
								</div>
														
														
								<div class="form__group">
									<label class="form-field__label--select">Tutor Support  <span style="color: red;">*</span></label>
									<select name="tutor_support" id="tutor_support" class="forminput class="form-field__input"auto" required="">
									<?php 
										if(!$tutorsupport) { ?>
											<option value="0">-Select-</option>
										<?php } else { ?>
											<option value="<?php echo $tutorsupport;?>"><?php echo $tutorsupport;?></option>
											<option value="0">-Select-</option>
										<?php } 
										
										for($i=1;$i<=10;$i++) {
											if($tutorsupport != $i) { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php }
										}
									?>
									</select>
								</div>
														
								<div class="form__group">
									<label class="form-field__label--select">Value for Money  <span style="color: red;">*</span></label>
									<select name="value_for_money" id="value_for_money" >
									<?php 
										if(!$valueformoney) { ?>
											<option value="0">-Select-</option>
										<?php } else { ?>
											<option value="<?php echo $valueformoney;?>"><?php echo $valueformoney;?></option>
											<option value="0">-Select-</option>
										<?php } 
										for($i=1;$i<=10;$i++) {
											if($valueformoney != $i) { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option><?php
											}
										}
										?>
									</select>
								</div>

							</fieldset>
							
							<fieldset>         
														
								<div class="form__group">
									<label><strong>Please write a short testimonial</strong> that sums up your training experience with ETS, your feelings about the course you have just completed and the support you received.</label>
									<textarea name="review" cols="40" rows="2" placeholder="Please write your testimonial here."><?php echo $review;?></textarea>
								</div>
														
														
								<div class="form__group">
									<label class=""><strong><span class="textred">Do we have your permission</span></strong> to use this testimonial on our web site, <strong>quoting only your initials and town?</strong> <span style="color: red;">*</span></label> <br />
									<select name="cust_approve" required>
										<?php
											if(!$custapprove) { ?>
												<option value="yes">Yes</option>
												<option value="no">No</option>
											<?php } elseif($custapprove == 'yes') { ?>
												<option value="yes">Yes</option>
												<option value="no">No</option>
											<?php } else { ?>
												<option value="no">No</option>
												<option value="yes">Yes</option>
											<?php } ?>
									</select>
								</div>
							</fieldset> 
								
							<div>
								<button type="submit" form="feedbackform-course" class="btn btn--blue btn--hover-yellow btn--large" value="Submit">Submit</button><input class="form-field__input" type="hidden" name="new_hidden" value="add">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer(); ?>